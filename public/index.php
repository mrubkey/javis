<?php
//Error report
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED | E_STRICT));
ini_set('display_errors', 'on');

//Gzip compresses
ini_set('zlib_output_compression', 'On');
if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
    ob_start("ob_gzhandler");
else
    ob_start();

date_default_timezone_set("Asia/Saigon");

mb_internal_encoding('UTF-8');

//Set header charset
header("Content-Type: text/html; charset=utf-8", true);

//Run mode
defined('DEBUG') || define('DEBUG', true);

// Define path
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
defined('LIBRARY_PATH') || define('LIBRARY_PATH', "../library");
defined('APPLICATION_PATH') || define('APPLICATION_PATH', '../application');
defined('STORAGE_PATH') || define('STORAGE_PATH', '../storage');

//Domain
defined('DOMAIN') || define('DOMAIN', 'http://javis.local');

require_once '../library/Javis/Loader.php';
\Javis\Loader::registerAutoload();

$controller = new \Javis\Controller\Front();

//Dispatch application
$errorFunction = array(
    404 => function (\Javis\Router &$router) {
        $router->action = "index";
        $router->controller = "Error";
        $router->module = "Frontend";
        $router->args = array('code' => 404);
    }
);

//Dispatch
$controller->dispatch($errorFunction);
