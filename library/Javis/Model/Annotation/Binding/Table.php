<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Table.php 1 2015-11-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Binding;

/**
 * Table
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_binding_table.html
 * @Annotation
 * @Target("CLASS")
 */
class Table extends BindingAbstract
{
    /**
     * Table name
     *
     * @var string
     */
    public $table;

    /**
     * Set params of annotation
     *
     * @param $properties
     *
     * @throws \Exception
     */
    public function init($properties)
    {
        if (isset($properties[0]) && is_string($properties[0])) {
            $this->table = $properties[0];
        } else {
            if (isset($properties['table']) && is_string($properties['table'])) {
                $this->table = $properties['table'];
            } else {
                throw new \Exception('Binding\Table parameter table name string');
            }
        }
    }

    /**
     * Binding
     */
    public function onBinding()
    {
        $this->_model->_name = $this->table;
        $this->_model->db->setTable($this->table);
    }
}