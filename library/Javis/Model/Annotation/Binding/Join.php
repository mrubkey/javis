<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Join.php 1 2015-11-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Binding;

/**
 * Join
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_binding_join.html
 * @Annotation
 * @Target("CLASS")
 * @Target("PROPERTY")
 */
class Join extends BindingAbstract
{
    /**
     * Table name
     *
     * @var string
     */
    public $table;

    /**
     * Model name
     *
     * @var string
     */
    public $model;

    /**
     * Field name
     * @var string
     */
    public $field;

    /**
     * Relation
     * MANY_MANY HAS_MANY HAS_ONE
     *
     * @var string
     */
    public $relation;

    /**
     * Set params of annotation
     *
     * @param $properties
     */
    public function init($properties)
    {
        if (isset($properties['table']) || isset($properties[0]))
            $this->table = $properties['table'];
        if (isset($properties['field']))
            $this->field = $properties['field'];
        else
            $this->field = 'id';
        if (isset($properties['relation']))
            $this->relation = $properties['relation'];
        else
            $this->relation = 'HAS_ONE';
    }

    public function onBinding() {}
}