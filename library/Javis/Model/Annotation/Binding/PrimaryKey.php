<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: PrimaryKey.php 1 2015-11-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Binding;

/**
 * PrimaryKey
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_binding_primarykey.html
 * @Annotation
 * @Target("CLASS")
 */
class PrimaryKey extends BindingAbstract
{
    /**
     * PrimaryKey name
     *
     * @var string
     */
    public $primaryKey;

    /**
     * Set params of annotation
     *
     * @param $properties
     *
     * @throws \Exception
     */
    public function init($properties)
    {
        if (isset($properties[0]) && is_string($properties[0])) {
            $this->primaryKey = $properties[0];
        } else {
            if (isset($properties['primaryKey']) && is_string($properties['primaryKey'])) {
                $this->primaryKey = $properties['primaryKey'];
            } else {
                throw new \Exception('PrimaryKey parameter primary key name string');
            }
        }
    }

    /**
     * Binding
     */
    public function onBinding()
    {
        $this->_model->_primaryKey = $this->primaryKey;
    }
}