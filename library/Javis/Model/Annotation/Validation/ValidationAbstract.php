<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: ValidationAbstract.php 1 2015-14-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Validation;


use Javis\Model\Annotation\AnnotationAbstract;


/**
 * Class ValidationAbstract
 *
 * @author        Mr.UBKey
 * @link          http://javis.xweb.vn/user_guide/javis_model_annotation_validation_validationabstract.html
 */
abstract class ValidationAbstract extends AnnotationAbstract
{
    /**
     * Name rule in jQuery Validation
     *
     * @var string
     */
    public $name;

    /**
     * Error message
     *
     * @var string
     */
    public $message;

    /**
     * Property name
     *
     * @var string
     */
    protected $property;

    /**
     * Set property name
     *
     * @param $propertyName
     */
    public function setProperty($propertyName)
    {
        $this->property = $propertyName;
    }

    abstract function onValidate();
}