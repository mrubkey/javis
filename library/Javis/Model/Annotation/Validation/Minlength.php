<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Minlength.php 1 2015-15-09 04:05:09 PM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Validation;

/**
 * Minlength Model Validation
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_validation_minlength.html
 * @Annotation
 * @Target("PROPERTY")
 */
class Minlength extends ValidationAbstract
{
    /**
     * Min length
     *
     * @var int
     */
    public $minlength;

    /**
     * Set params of annotation
     *
     * @param $properties
     *
     * @throws \Exception
     */
    public function init($properties)
    {
        //validation name
        $this->name = "minlength";

        if (isset($properties[0]) && is_int($properties[0])) {
            $this->minlength = $properties[0];
        } else {
            if (isset($properties['minlength']) && is_int($properties['minlength'])) {
                $this->minlength = $properties['minlength'];
            } else {
                throw new \Exception('Minlength must have parameter type int');
            }
        }
        if (isset($properties['message']) && is_string($properties['message'])) {
            $this->message = $properties['message'];
        } //default message
        else {
            $this->message = "{$this->property} min {$this->minlength} character.";
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see \XPHP\Model\Attribute\Validation\Abstract::validate()
     */
    public function onValidate()
    {
        //Check
        $property = $this->property;
        return !(strlen($this->_model->$property) < $this->minlength);
    }
}