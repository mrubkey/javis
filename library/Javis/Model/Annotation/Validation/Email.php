<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Email.php 1 2015-15-09 04:05:09 PM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Validation;

/**
 * Email Model Validation
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_validation_required.html
 * @Annotation
 * @Target("PROPERTY")
 */
class Email extends ValidationAbstract
{
    /**
     * Set params of annotation
     *
     * @param $properties
     */
    public function init($properties)
    {
        //validation name
        $this->name = "email";

        if (isset($properties['message']) && is_string($properties['message']))
            $this->message = $properties['message'];
        //default message
        else
            $this->message = "{$this->property} is not email valid.";
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Javis\Model\Annotation\Validation\Abstract::validate()
     */
    public function onValidate()
    {
        //Check
        $property = $this->property;
        return preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $this->_model->$property);
    }
}