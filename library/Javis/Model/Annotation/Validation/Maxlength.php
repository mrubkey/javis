<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Maxlength.php 1 2015-15-09 04:05:09 PM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Validation;

/**
 * Maxlength Model Validation
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_validation_maxlength.html
 * @Annotation
 * @Target("PROPERTY")
 */
class Maxlength extends ValidationAbstract
{
    /**
     * Max length
     *
     * @var int
     */
	public $maxlength;

    /**
     * Set params of annotation
     *
     * @param $properties
     *
     * @throws \Exception
     */
    public function init ($properties)
    {
    	//validation name
    	$this->name = "maxlength";

        if (isset($properties[0]) && is_int($properties[0])) {
            $this->maxlength = $properties[0];
        } else {
            if (isset($properties['maxlength']) && is_int($properties['maxlength'])) {
                $this->maxlength = $properties['maxlength'];
            } else {
                throw new \Exception('Maxlength must have parameter type int');
            }
        }
        if (isset($properties['message']) && is_string($properties['message']))
            $this->message = $properties['message'];
        //default message
		else
			$this->message = "{$this->property} max {$this->maxlength} character.";
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Javis\Model\Annotation\Validation\Abstract::validate()
     */
	public function onValidate()
	{
		//Check
		$property = $this->property;
		return !(strlen($this->_model->$property) > $this->maxlength);
	}
}