<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Callback.php 1 2015-15-09 04:05:09 PM Mr.UBKey $
 */

namespace Javis\Model\Annotation\Validation;

/**
 * Callback Model Validation
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_model_annotation_validation_callback.html
 * @Annotation
 * @Target("PROPERTY")
 */
class Callback extends ValidationAbstract
{
    /**
     * Callback name
     *
     * @var string
     */
    public $callback;

    /**
     * Set params of annotation
     *
     * @param $properties
     *
     * @throws \Exception
     */
    public function init($properties)
    {
        //validation name
        $this->name = "callback";

        if (isset($properties['callback']) && is_string($properties['callback'])) {
            $this->callback = $properties['callback'];
        } else {
            if (isset($properties[0]) && is_string($properties[0])) {
                $this->callback = $properties[0];
            } else {
                throw new \Exception('Callback must have parameter type string callback name');
            }
        }

        if (isset($properties['message']) && is_string($properties['message'])) {
            $this->message = $properties['message'];
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see \XPHP\Model\Attribute\Validation\Abstract::validate()
     */
    public function onValidate()
    {
        //Return
        $result = false;

        //Check
        $property = $this->property;
        $callback = $this->callback;

        if (!method_exists($this->_model, $callback))
            throw new \Exception("Can't find {$callback}() in " . get_class($this->_model) . ".");

        $callbackResult = $this->_model->$callback($this->_model->$property);

        //Check result type
        if (is_bool($callbackResult))
            $result = $callbackResult;
        else if (is_array($callbackResult)) {
            if (!isset($callbackResult[0]) || !is_bool($callbackResult[0]))
                throw new \Exception("Result of {$callback}() is not valid.");
            else {
                $result = $callbackResult[0];
            }
            //Return error message in callback
            if (isset($callbackResult['message']))
                $this->message = $callbackResult['message'];
        }

        //default message
        if (empty($this->message))
            $this->message = "{$this->property} is not valid.";

        return $result;
    }
}