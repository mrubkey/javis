<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package             Javis
 * @author              XWEB Dev Team
 * @copyright           Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license             http://xphp.xweb.vn/license.html     GNU GPL License
 * @version             $Id: Html.php 1 2015-22-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis;

use Javis\Controller\Front;
use Javis\View\Partial;

/**
 * Class Html
 *
 * @author          Mr.UBKey
 * @link            http://xphp.xweb.vn/user_guide/xphp_html.html
 */
class Html
{
    /**
     * Result View
     *
     * @var View
     */
    private $_actionViewResult;

    /**
     * Model
     *
     * @var Model
     */
    private $_model;

    /**
     * Router
     *
     * @var Router
     */
    private $_router;

    /**
     * @param Router $router
     * @param View $actionViewResult
     */
    public function __construct($router = NULL, $actionViewResult = NULL)
    {
        if ($router !== NULL)
            $this->_router = $router;
        if ($actionViewResult !== NULL) {
            $this->_actionViewResult = $actionViewResult;
            $this->_model = &$actionViewResult->model;
        }
    }

    /**
     * Load CSS
     *
     * @param $path
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function loadCss($path)
    {
        if (is_string($path)) {
            if (is_file($path))
                echo '<link href="' . Url::getRelativeUrl() . '/' . $path .
                    '" rel="stylesheet" type="text/css" />';
            else
                throw new \Exception("Can't find css file " . Url::getRelativeUrl() . '/' . $path);
        } else
            if (is_array($path)) {
                foreach ($path as $p) {
                    if (is_file($p))
                        echo '<link href="' . Url::getRelativeUrl() . '/' .
                            $p . '" rel="stylesheet" type="text/css" />';
                    else
                        throw new \Exception("Can't find css file " . Url::getRelativeUrl() . '/' . $p);
                }
            }
        return $this;
    }

    /**
     * Load JS
     *
     * @param $path
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function loadJs($path)
    {
        if (is_string($path)) {
            if (is_file($path))
                echo '<script language="javascript" src="' .
                    Url::getRelativeUrl() . '/' . $path .
                    '" type="text/javascript"></script>';
            else
                throw new \Exception("Can't find js file " . Url::getRelativeUrl() . '/' . $path);
        } else
            if (is_array($path)) {
                foreach ($path as $p) {
                    if (is_file($p))
                        echo '<script language="javascript" src="' .
                            Url::getRelativeUrl() . '/' . $p .
                            '" type="text/javascript"></script>';
                    else
                        throw new \Exception(
                            "Can't find js file " .
                            Url::getRelativeUrl() . '/' . $p);
                }
            }
        return $this;
    }

    /**
     * Overload method action.
     */
    function __call($method_name, $arguments)
    {
        //we inspect number of arguments
        if ($method_name == "renderAction" && count($arguments) == 1) {
            return $this->renderAction1($arguments[0]);
        } else
            if ($method_name == "renderAction" && count($arguments) == 2 &&
                is_string($arguments[1])
            ) {
                return $this->renderAction2($arguments[0], $arguments[1]);
            } else
                if ($method_name == "renderAction" && count($arguments) == 2 &&
                    is_array($arguments[1])
                ) {
                    return $this->renderAction5($arguments[0], $arguments[1]);
                } else
                    if ($method_name == "renderAction" && count($arguments) == 3 &&
                        is_string($arguments[2])
                    ) {
                        return $this->renderAction3($arguments[0],
                            $arguments[1], $arguments[2]);
                    } else
                        if ($method_name == "renderAction" &&
                            count($arguments) == 3 && is_array($arguments[2])
                        ) {
                            return $this->renderAction6($arguments[0],
                                $arguments[1], $arguments[2]);
                        } else
                            if ($method_name == "renderAction" &&
                                count($arguments) == 4
                            ) {
                                return $this->renderAction4($arguments[0],
                                    $arguments[1], $arguments[2], $arguments[3]);
                            }
    }

    function renderAction1($action)
    {
        $router = Router::cloneInstance();
        $router->action = $action;

        //Execute request
        $frontController = new Front($router);
        $frontController->dispatch();
    }

    function renderAction2($action, $controller)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;

        //Execute request
        $frontController = new Front($router);
        $frontController->dispatch();
    }

    function renderAction3($action, $controller, $module)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        $router->module = $module;

        //Execute request
        $frontController = new Front($router);
        $frontController->dispatch();
    }

    function renderAction4($action, $controller, $module, $args)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        $router->module = $module;
        $router->args = $args;

        //Execute request
        $frontController = new Front($router);
        $frontController->dispatch();
    }

    function renderAction5($action, $args)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->args = $args;

        //Execute request
        $frontController = new Front($router);
        $frontController->dispatch();
    }

    function renderAction6($action, $controller, $args)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        $router->args = $args;

        //Execute request
        $frontController = new Front($router);
        $frontController->dispatch();
    }

    public function renderContent($url)
    {
        echo file_get_contents($url);
    }

    /**
     * Render partial view
     *
     * @param $file
     * @param null $data
     * @param null $model
     */
    public function renderPartial($file, $data = null, $model = null)
    {
        $partial = new Partial();
        //router
        $partial->router = $this->_router;
        //data
        $partial->data = $data;
        //model
        $partial->model = $model;
        //file name
        $view = new View($this->_router);
        $folder = $view->getViewFolder();
        $partial->file = "{$folder}/{$file}.phtml";
        //Render
        $partial->render();
    }

    public function stringEncode($html)
    {
        return String::encodeHtmlString($html);
    }

    public function stringDecode($html)
    {
        return String::decodeHtmlString($html);
    }

    public function encode($html)
    {
        return String::encodeHtmlString($html);
    }

    public function decode($html)
    {
        return String::decodeHtmlString($html);
    }

    public function raw($html_stringencoded)
    {
        echo $this->stringDecode($html_stringencoded);
    }
}