<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Form.php 1 2015-22-09 15:08:09 PM Mr.UBKey $
 */

namespace Javis;


use Javis\Html\Base;
use Javis\Html\Validation;
use Javis\Html\ValidationUnobtrusive;

/**
 * Form helper for view
 *
 * @author        Mr.UBKey
 * @author        BuiPhong
 * @link          http://javis.xweb.vn/user_guide/javis_form.html
 */
class Form
{
    /**
     * Model
     *
     * @var Model
     */
    protected $model;

    /**
     * Router
     *
     * @var Router
     */
    private $_router;

    /**
     * javascript code
     *
     * @var bool
     */
    private $_script = "";

    /**
     * Form name
     */
    private $_formName;

    /**
     * Enable client validation
     *
     * @var bool
     */
    public $enableClentSideValidation = true;

    /**
     * Enable unobtrusive for client validation
     *
     * @var bool
     */
    public $enableUnobtrusive = true;

    /**
     * @var ValidationUnobtrusive
     */
    private $_validationUnobtrusive;

    /**
     * @param null $router
     * @param null $actionResultView
     */
    public function __construct($router = null, $actionResultView = null)
    {
        if ($router !== null)
            $this->_router = $router;
        if ($actionResultView !== null)
            $this->model = &$actionResultView->model;
        if ($this->enableUnobtrusive && $this->enableClentSideValidation)
            $this->_validationUnobtrusive = new ValidationUnobtrusive($actionResultView);
    }

    /**
     * Begin new form
     *
     * @param null $name
     * @param array $attrs
     */
    public function begin($name = null, $attrs = array())
    {
        if ($name == null)
            $name = String::randomString(5);
        if (!isset($attrs["method"]))
            $attrs["method"] = "post";
        if (!isset($attrs["id"]))
            $attrs["id"] = $name;
        //set form name
        $this->_formName = $attrs["id"];
        //get attribute string
        $attr = Base::htmlAttributes($attrs);
        echo "<form name='{$this->_formName}' $attr>";
    }

    /**
     * End form
     */
    public function end()
    {
        echo "</form>";
    }

    /**
     * Begin form with ajax
     *
     * @param array $ajaxOption
     * @param null $name
     * @param array $attrs
     */
    public function beginAjax($ajaxOption = array(), $name = null, $attrs = array())
    {
        //Parse ajax option set to attribute
        $attrs['data-ajax'] = "true";
        foreach ($ajaxOption as $ajaxKey => $ajaxValue) {
            $option = "data-ajax-" . $ajaxKey;
            $attrs[$option] = $ajaxValue;
        }
        $this->begin($name, $attrs);
    }

    /**
     * Label
     *
     * @param $text
     * @param null $attrs
     */
    public function label($text, $attrs = null)
    {
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<label $attr>$text</label>";
        } else
            $html = "<label>$text</label>";
        echo $html;
    }

    /**
     * Label for model
     *
     * @param $property
     * @param null $attrs
     *
     * @throws \Exception
     */
    public function labelFor($property, $attrs = null)
    {
        //check property in model
        $this->_propertyExistsModel($property);
        if (!isset($attrs["for"]))
            $attrs["for"] = $property;
        //get annotation label
        $labelAnnotation = Annotation::ofProperty($this->model, $property, '\\Javis\\Model\\Annotation\\Html\\Label');
        if (isset($labelAnnotation[0]))
            $this->label($labelAnnotation[0]->text, $attrs);
        //default name of property
        else
            $this->label($property, $attrs);
    }

    /**
     * Textbox
     *
     * @param $name
     * @param null $value
     * @param null $attrs
     */
    public function textbox($name, $value = null, $attrs = null)
    {
        //set va;ie
        if ($value !== null)
            $attrs["value"] = $value;
        //set default id as name
        if (!isset($attrs["id"]))
            $attrs["id"] = $name;
        if (!isset($attrs["name"]))
            $attrs["name"] = $name;

        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<input type='text' $attr/>";
        } else
            $html = "<input name='$name' type='text' />";
        echo $html;
    }

    /**
     * Textbox for model
     *
     * @param $property
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function textboxFor($property, $attrs = array())
    {
        //check property in model
        $this->_propertyExistsModel($property);
        //check unobtrusive
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        if (!empty($this->model->$property))
            $attrs["value"] = $this->model->$property;
        $this->textbox($property, null, $attrs);
    }

    /**
     * Password
     *
     * @param $name
     * @param null $value
     * @param null $attrs
     */
    public function password($name, $value = null, $attrs = null)
    {
        //default value
        if ($value !== null)
            $attrs["value"] = $value;
        //set id as name
        if (!isset($attrs["id"]))
            $attrs["id"] = $name;
        if (!isset($attrs["name"]))
            $attrs["name"] = $name;
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<input type='password' $attr/>";
        } else
            $html = "<input name='$name' type='password' />";
        echo $html;
    }

    /**
     * Password for model
     *
     * @param $property
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function passwordFor($property, $attrs = array())
    {
        //check property in model
        $this->_propertyExistsModel($property);
        //check unobtrusive
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        if (!empty($this->model->$property))
            $attrs["value"] = $this->model->$property;
        $this->password($property, null, $attrs);
    }

    /**
     * Textarea
     *
     * @param $name
     * @param null $value
     * @param null $attrs
     */
    public function textarea($name, $value = null, $attrs = null)
    {
        //set id as name
        if (!isset($attrs["id"]))
            $attrs["id"] = $name;
        if (!isset($attrs["name"]))
            $attrs["name"] = $name;
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<textarea $attr>" . $value .
                "</textarea>";
        } else
            $html = "<textarea name='$name'>" . $value . "</textarea>";
        echo $html;
    }

    /**
     * Textarea for model
     *
     * @param $property
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function textareaFor($property, $attrs = array())
    {
        //check property in model
        $this->_propertyExistsModel($property);
        //check unobtrusive
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs, $this->_validationUnobtrusive->get($property));
        $value = "";
        if (!empty($this->model->$property))
            $value = $this->model->$property;
        $this->textarea($property, $value, $attrs);
    }

    /**
     * Hidden
     *
     * @param $name
     * @param null $value
     * @param null $attrs
     */
    public function hidden($name, $value = null, $attrs = null)
    {
        //default value
        if ($value !== null)
            $attrs["value"] = $value;
        //set id as name
        if (!isset($attrs["id"]))
            $attrs["id"] = $name;
        if (!isset($attrs["name"]))
            $attrs["name"] = $name;
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<input type='hidden' $attr/>";
        } else
            $html = "<input name='$name' type='hidden' />";
        echo $html;
    }

    /**
     * Hidden for model
     *
     * @param $property
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function hiddenFor($property, $attrs = array())
    {
        $this->_propertyExistsModel($property);
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        if (!empty($this->model->$property))
            $attrs["value"] = $this->model->$property;
        $this->hidden($property, null, $attrs);
    }

    /**
     * Checkbox
     *
     * @param $name
     * @param null $value
     * @param null $attrs
     */
    public function checkbox($name, $value = null, $attrs = null)
    {
        if ($value !== null) {
            if (is_bool($value))
                $attrs["checked"] = "checked";
            else
                $attrs["value"] = $value;
        }
        $attrs["value"] = "true";
        if (!isset($attrs["id"]))
            $attrs["id"] = $name;
        if (!isset($attrs["name"]))
            $attrs["name"] = $name;
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<input type='checkbox' $attr />";
        } else
            $html = "<input type='checkbox' name='$name' />";
        echo $html;
    }

    /**
     * Checkbox for model
     *
     * @param $property
     * @param array $attrs
     * @throws \Exception
     */
    public function checkboxFor($property, $attrs = array())
    {
        $this->_propertyExistsModel($property);
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        $value = null;
        if (!empty($this->model->$property))
            $value = $this->model->$property;
        if ($value || $value == "true")
            $attrs["checked"] = "checked";
        $this->checkbox($property, $value, $attrs);
    }

    /**
     * Checkgroup
     *
     * @param $name
     * @param $arrOptions
     * @param null $value
     * @param array $attrs
     */
    public function checkgroup($name, $arrOptions, $value = null, $attrs = array())
    {
        $html = "<ul>";
        $i = 1;
        if ($value !== null) {
            if (is_string($value)) {
                $value = explode(',', $value);
            }
        }
        foreach ($arrOptions as $key => $option) {
            $attr = $attrs;
            if ($value !== null) {
                if (in_array($key, $value))
                    $attr['checked'] = 'checked';
            }
            $attr = Base::htmlAttributes($attr);
            $html .= "<li><input id='{$name}{$i}' type='checkbox' name='{$name}[]' value='{$key}' $attr /><label for='{$name}{$i}'>{$option}</label></li>";
            $i++;
        }
        $html .= "</ul>";
        echo $html;
    }

    /**
     * Checkgroup for model
     *
     * @param $property
     * @param $arrOptions
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function checkgroupFor($property, $arrOptions, $attrs = array())
    {
        $this->_propertyExistsModel($property);
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        $value = null;
        if (isset($this->model->$property))
            $value = $this->model->$property;
        $this->checkgroup($property, $arrOptions, $value, $attrs);
    }

    /**
     * Radio
     *
     * @param $name
     * @param $arrOptions
     * @param null $value
     *
     * @param array $attrs
     */
    public function radio($name, $arrOptions, $value = null, $attrs = array())
    {
        $html = "<ul>";
        $i = 1;
        foreach ($arrOptions as $key => $option) {
            $attr = $attrs;
            if ($value !== null) {
                if ($key == $value)
                    $attr['checked'] = 'checked';
            }
            $attr = Base::htmlAttributes($attr);
            $html .= "<li><input type='radio' id='{$name}{$i}' name='{$name}' value='{$key}' $attr> <label for='{$name}{$i}'>{$option}</label></li>";
            $i++;
        }
        $html .= "</ul>";
        echo $html;
    }

    /**
     * Radio for model
     *
     * @param $property
     * @param $arrOptions
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function radioFor($property, $arrOptions, $attrs = array())
    {
        $this->_propertyExistsModel($property);
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        $value = null;
        if (isset($this->model->$property))
            $value = $this->model->$property;
        $this->radio($property, $arrOptions, $value, $attrs);
    }

    /**
     * Select
     *
     * @param $name
     * @param $arrOptions
     * @param null $value
     * @param null $valueID
     * @param null $nameID
     * @param null $attrs
     */
    public function select($name, $arrOptions, $value = null, $valueID = null, $nameID = null,
                           $attrs = null)
    {
        if ($attrs !== null) {
            $attr = Base::htmlAttributes($attrs);
            if (isset($attrs['id']))
                $html = "<select name='$name' $attr >";
            else
                $html = "<select name='$name' id='$name' $attr >";
        } else {
            $html = "<select name='$name' id='$name'>";
        }
        foreach ($arrOptions as $key => $option) {
            if ($valueID == null || $nameID == null)
                if ($value == $key)
                    $html .= '<option value="' . $key . '" selected="selected">' .
                        $option . '</option>';
                else
                    $html .= '<option value="' . $key . '">' . $option .
                        '</option>';
            else
                if ($value == $option[$valueID])
                    $html .= '<option value="' . $option[$valueID] .
                        '" selected="selected">' . $option[$nameID] . '</option>';
                else
                    $html .= '<option value="' . $option[$valueID] . '">' .
                        $option[$nameID] . '</option>';
        }
        $html .= "</select>";
        echo $html;
    }

    /**
     * Select for model
     *
     * @param $property
     * @param $arrOptions
     * @param null $valueID
     * @param null $nameID
     * @param array $attrs
     *
     * @throws \Exception
     */
    public function selectFor($property, $arrOptions, $valueID = null,
                              $nameID = null, $attrs = array())
    {
        $this->_propertyExistsModel($property);
        if ($this->enableUnobtrusive)
            $attrs = array_merge($attrs,
                $this->_validationUnobtrusive->get($property));
        $value = null;
        if (isset($this->model->$property))
            $value = $this->model->$property;
        $this->select($property, $arrOptions, $value, $valueID, $nameID, $attrs);
    }

    /**
     * Submit
     *
     * @param null $name
     * @param null $value
     * @param null $attrs
     */
    public function submit($name = null, $value = null, $attrs = null)
    {
        if ($name !== null)
            $attrs["name"] = $name;
        if ($value !== null)
            $attrs["value"] = $value;
        if (!isset($attrs["id"]) && isset($attrs["name"]))
            $attrs["id"] = $attrs["name"];
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<input name='$name' type='submit' $attr/>";
        } else
            $html = "<input name='$name' type='submit' />";
        echo $html;
    }

    /**
     * Reset
     *
     * @param null $name
     * @param null $value
     * @param null $attrs
     */
    public function reset($name = null, $value = null, $attrs = null)
    {
        if ($name !== null)
            $attrs["name"] = $name;
        if ($value !== null)
            $attrs["value"] = $value;
        if (!isset($attrs["id"]) && isset($attrs["name"]))
            $attrs["id"] = $attrs["name"];
        if ($attrs !== null && is_array($attrs)) {
            $attr = Base::htmlAttributes($attrs);
            $html = "<input type='reset' $attr/>";
        } else
            $html = "<input type='reset' />";
        echo $html;
    }

    /**
     * Show validation message
     *
     * @param $property
     * @param null $attrs
     */
    public function validationMessage($property, $attrs = null)
    {
        $errorMess = "";
        $messages = $this->model->getErrorMessage();
        if (!empty($messages)) {
            if (isset($messages[$property]) && sizeof($messages[$property]) > 0) {
                sort($messages[$property], SORT_ASC);
                $errorMess = $messages[$property][0];
            }
        }
        echo "<span class=\"field-validation-valid\" data-valmsg-for=\"{$property}\" data-valmsg-replace=\"true\">
			  {$errorMess}
			  </span>";
    }

    /**
     * Show script
     */
    public function scriptRegistrar()
    {
        if ($this->enableClentSideValidation) {
            $validation = new Validation($this->model);
            $validation->setFormName($this->_formName);
            $this->_script .= $validation->getScript();
            print $this->_script;
        }
    }

    /**
     * Check properties exists in model
     *
     * @param $property
     *
     * @throws \Exception
     */
    private function _propertyExistsModel($property)
    {
        if (!property_exists($this->model, $property)) {
            throw new \Exception("Can't find property $" . $property . " in model " . get_class($this->model));
        }
    }
}