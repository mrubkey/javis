<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Url.php 1 2015-10-09 02:05:09 PM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Url
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_url.html
 */
class Url
{
    /**
     * @var Router
     */
    public $_router;

    /**
     * @param Router $router
     */
    public function __construct($router = NULL)
    {
        $this->_router = $router;
    }

    /**
     * Get path to file
     *
     * @param $path
     * @param bool|false $check_exist
     * @return string
     *
     * @throws \Exception
     */
    public function content($path, $check_exist = false)
    {
        if ($check_exist) {
            if (!is_file($path)) {
                throw new \Exception("Không tìm thấy tệp tin tại " . $path);
            }
        }
        return self::getApplicationUrl() . "/" . $path;
    }

    /**
     * Overload method action and geturl
     */
    function __call($method_name, $arguments)
    {
        //we inspect number of arguments
        if ($method_name == "action" && count($arguments) == 1) {
            return $this->action1($arguments[0]);
        } else if ($method_name == "action" && count($arguments) == 2
            && is_string($arguments[1])
        ) {
            return $this->action2($arguments[0], $arguments[1]);
        } else if ($method_name == "action" && count($arguments) == 2
            && is_array($arguments[1])
        ) {
            return $this->action5($arguments[0], $arguments[1]);
        } else if ($method_name == "action" && count($arguments) == 3
            && is_string($arguments[2])
        ) {
            return $this->action3(
                $arguments[0], $arguments[1],
                $arguments[2]
            );
        } else if ($method_name == "action" && count($arguments) == 3
            && is_array($arguments[2])
        ) {
            return $this->action6(
                $arguments[0], $arguments[1],
                $arguments[2]
            );
        } else if ($method_name == "action"
            && count($arguments) == 4
        ) {
            return $this->action4(
                $arguments[0],
                $arguments[1], $arguments[2], $arguments[3]
            );
        } //application
        else if ($method_name == "getUrl"
            && count($arguments) == 0
        ) {
            return $this->getUrl1();
        } else if ($method_name == "getUrl"
            && count($arguments) == 1
        ) {
            return $this->getUrl2($arguments[0]);
        }
    }

    function action1($action)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        //Thiết lập args rỗng
        $router->args = array();
        return self::getActionUrl($router);
    }

    function action2($action, $controller)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        //Thiết lập args rỗng
        $router->args = array();
        return self::getActionUrl($router);
    }

    function action3($action, $controller, $module)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        $router->module = $module;
        //Thiết lập args rỗng
        $router->args = array();
        return self::getActionUrl($router);
    }

    function action4($action, $controller, $module, $args)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        $router->module = $module;
        $router->args = $args;
        return self::getActionUrl($router);
    }

    function action5($action, $args)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->args = $args;
        return self::getActionUrl($router);
    }

    function action6($action, $controller, $args)
    {
        $router = Router::cloneInstance();
        $router->action = $action;
        $router->controller = $controller;
        $router->args = $args;
        return self::getActionUrl($router);
    }

    /**
     * Get url
     *
     * @param null $path
     *
     * @return string
     */
    function getUrl($path = NULL)
    {
        if ($path === NULL)
            return self::getApplicationUrl();
        else {
            if (strpos($path, self::getRelativeUrl()) === false) {
                return self::getApplicationUrl() . '/' . $path;
            } else if (strpos($path, self::getRelativeUrl()) == 0) {
                $url = "";
                if (!empty($_SERVER['HTTPS'])) {
                    $url .= "https://" . $_SERVER['SERVER_NAME'];
                } else {
                    $url .= "http://" . $_SERVER['SERVER_NAME'];
                }
                return $url . $path;
            }
        }
    }

    /**
     * Get application url
     *
     * @return string
     */
    public static function getApplicationUrl()
    {
        $url = "";
        if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER["HTTPS"]) == "on") {
            $url .= "https://" . $_SERVER['SERVER_NAME'];
        } else {
            $url .= "http://" . $_SERVER['SERVER_NAME'];
        }
        //$url .= self::getRelativeUrl();
        return $url;
    }

    /**
     * Get absolute url
     *
     * @param null $url
     *
     * @return string
     */
    public static function getAbsoluteUrl($url = null)
    {
        if ($url) {
            return self::getApplicationUrl() . '/' . trim($url, '/');
        } else {
            return self::getApplicationUrl();
        }
    }

    /**
     * Get relative url
     *
     * @return string
     */
    public static function getRelativeUrl()
    {
        $phpself = explode('/', $_SERVER['PHP_SELF']);
        $url = "";
        for ($i = 0; $i < sizeof($phpself); $i++) {
            if ($i != 0 && $i != sizeof($phpself) - 1) {
                $url .= '/' . $phpself[$i];
            }
        }
        return $url;
    }

    /**
     * Get absolute url to action
     *
     * @param $ro
     * @param bool|false $origin
     *
     * @return string
     */
    public static function getAbsoluteActionUrl($ro, $origin = false)
    {
        return self::getAbsoluteUrl(self::getActionUrl($ro, $origin));
    }

    /**
     * Get action url
     *
     * @param $ro
     * @param bool|false $origin
     *
     * @return string
     */
    public static function getActionUrl($ro, $origin = false)
    {
        //Get instance from router
        $router = Router::newInstance($ro);

        if (!$origin) {
            //Check with rewrite
            if (Rewrite::$enable) {
                $url = self::getActionUrlRewrite($router);
                if ($url != null) {
                    return $url;
                }
            }
        }

        //absolute url
        $url = self::getAbsoluteUrl() . "/";

        //get value from router
        $module = $router->module;
        $args = $router->args;
        $controller = $router->controller;
        $action = $router->action;

        if ($module) {
            $url .= $module . '/';
        }

        //default value
        if ($controller
            && ($controller != "Index"
                || ($controller == "Index" && $action != "index"))
        ) {
            $url .= $controller;
        }
        if ($action != "index") {
            $url .= '/' . $action;
        }
        if (count($args) > 0) {
            $i = 1;
            foreach ($args as $key => $arg) {
                if ((string)$key != "module") {
                    if ($i == 1 && !empty($module) && ($action == "index") && ($controller == "Index")) {
                        $url .= $key . '/' . $arg;
                    } else {
                        $url .= '/' . $key . '/' . $arg;
                    }
                    $i++;
                }
            }
        }
        return trim($url, '/');
    }

    /**
     * Get action url with rewrite
     *
     * @param $ro
     *
     * @return null|string
     */
    public static function getActionUrlRewrite($ro)
    {
        Rewrite::newInstance($ro);
        return Rewrite::getUrl();
    }

    /**
     * Redirect by router
     *
     * @param $ro
     */
    public static function redirectAction($ro)
    {
        //Nếu tham số là một đối tượng (XPHP_Router) hoặc một mảng
        if (is_object($ro) || is_array($ro)) {
            self::redirectUrl(self::getActionUrl($ro));
        } //Nếu tham số là chuỗi
        else if (is_string($ro)) {
            self::redirectUrl(self::getRelativeUrl() . '/' . $ro);
        }
    }

    /**
     * Redirect by url
     *
     * @param $url
     */
    public static function redirectUrl($url)
    {
        if ($url) {
            header('Location:' . $url);
            exit();
        }
    }

    /**
     * Get current url
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER["HTTPS"]) == "on") {
            $url = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        } else {
            $url = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        }
        return $url;
    }

    /**
     * Encode url
     *
     * @param $url
     *
     * @return string
     */
    public function encode($url)
    {
        $url = str_replace('/', '~', $url);
        $url = str_replace('.', '$', $url);
        return urlencode($url);
    }

    /**
     * Decode url
     *
     * @param $url
     *
     * @return mixed|string
     */
    public function decode($url)
    {
        $url = urldecode($url);
        $url = str_replace('~', '/', $url);
        $url = str_replace('$', '.', $url);
        return $url;
    }

    /**
     * Get current url by current router
     *
     * @return string
     */
    public function current()
    {
        return self::getActionUrl($this->_router);
    }
}