<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author        XWEB Dev Team
 * @copyright    Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Layout.php 1 2015-22-06 02:05:09 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Layout
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_layout.html
 */
class Layout
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * File layout
     *
     * @var string
     */
    private $_layoutFile;

    /**
     * Folder layout
     *
     * @var string
     */
    private $_layoutFolder;

    /**
     * @param $router
     */
    public function __construct(&$router)
    {
        //Gán Router
        $this->router = &$router;
    }

    /**
     * Set layout file
     *
     * @param $layoutFile
     */
    public function setLayoutFile($layoutFile)
    {
        $this->_layoutFile = $layoutFile;
        $this->_detectLayoutFolder();
    }

    /**
     * Get layout file
     *
     * @return string
     */
    public function getLayoutFile()
    {
        return $this->_layoutFile;
    }

    /**
     * Get layout path
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getLayoutPath()
    {
        //Mobile path
        //$mobilePath = $this->getMobileLayoutPath();
        //if ($mobilePath)
            //$path = $mobilePath;
        //else

        $path = "{$this->_layoutFolder}/{$this->_layoutFile}.phtml";

        //return layout path
        if (is_file($path))
            return $path;
        else {
            throw new \Exception(
                "Can't find $path in =>
                 action = {$this->router->action} 
                 controller = {$this->router->controller} "
                    . ($this->router->module ? "module = {$this->router->module}" : "")
            );
        }
    }

    /**
     * Get path layout folder
     *
     * @return string
     */
    public function getLayoutFolder()
    {
        return $this->_layoutFolder;
    }

    /**
     * Detect layout folder
     */
    private function _detectLayoutFolder()
    {
        if (strpos($this->_layoutFile, "/") !== false) {
            $layouts = explode("/", $this->_layoutFile);
            $modulePath = Runtime::get('Router.Modules.' . strtolower($layouts[0]) . '.Map');
            $this->_layoutFile = $layouts[1];
        } else {
            $modulePath = Runtime::get('Router.Modules.' . strtolower($this->router->module) . '.Map');
        }

        $this->_layoutFolder = "{$modulePath}/Layouts";
    }

    /**
     * Get mobile layout path
     *
     * @return bool|string
     */
    public function getMobileLayoutPath()
    {
        //get application config
        $application = Config::get("Application");
        if ($application['mode'] == "mobile") {
            //Mobile path
            $mobilePath = "{$this->_layoutFolder}/{$this->_layoutFile}.mobile.phtml";
            //Device path
            $devicePath = "{$this->_layoutFolder}/{$this->_layoutFile}.{$application['detector']->device}.phtml";
            if (is_file($devicePath))
                return $devicePath;
            if (is_file($mobilePath))
                return $mobilePath;
        }
        return false;
    }
}
