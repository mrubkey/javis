<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         XPHP
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://xphp.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Rewrite.php 1 2015-26-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Rewrite
 *
 * @category        XPHP
 * @package         XPHP\Rewrite
 * @author          Mr.UBKey
 * @link            http://xphp.xweb.vn/user_guide/xphp_rewrite.html
 */
class Rewrite
{
    /**
     * Router instance
     *
     * @var Router
     */
    protected static $_router;

    /**
     * Rewrite enable
     *
     * @var bool
     */
    public static $enable;

    /**
     * Path to file contains routes
     *
     * @var string
     */
    public static $file;

    /**
     * Routes
     *
     * @var array
     */
    private static $_routes = array();

    /**
     * Urls
     *
     * @var array
     */
    private static $_urls;

    /**
     * Flag init called
     *
     * @var bool
     */
    private static $_init = false;

    /**
     * Create new instance
     *
     * @param $router
     */
    public static function newInstance(&$router)
    {
        static::$_init = false;
        static::init($router);
    }

    /**
     * Init
     *
     * @param $router
     */
    public static function init(&$router)
    {
        if (!static::$_init) {
            //Get rewrite config
            $config = Config::get('Application.Rewrite');

            //Rewrite enable
            if (static::$enable = $config['enable']) {
                static::$_router = $router;
                $files = array();

                Runtime::load('Rewrite', function ($time) use ($config, &$files) {
                    $mods = array();
                    foreach ($config['files'] as $f) {
                        if (file_exists($path = APPLICATION_PATH . '/' . $f . '.php')) {
                            $files[] = $path;
                            $mods[] = filemtime($path);
                        }
                    }
                    arsort($mods);
                    return $time >= array_shift($mods);
                });
                $rewrite = Runtime::get('Rewrite');
                if (!$rewrite) {
                    //Get data
                    foreach ($files as $path)
                        static::$_routes = array_merge(static::$_routes, require $path);
                    //Get all url
                    $urls = array();
                    foreach (static::$_routes as $r) {
                        $urls[(isset($r['module']) ? $r['module'] : "") . "/{$r['controller']}/{$r['action']}"] = $r['url'];
                    }
                    static::$_urls = $urls;

                    //Save runtime
                    Runtime::set('Rewrite', array('enable' => static::$enable, 'routes' => static::$_routes, 'urls' => $urls));
                    Runtime::save('Rewrite');
                } else {
                    static::$enable = $rewrite['enable'];
                    static::$_routes = $rewrite['routes'];
                    static::$_urls = $rewrite['urls'];
                }
            }

            static::$_init = true;
        }
    }

    /**
     * Process rewrite
     *
     * @return bool
     */
    public static function processing()
    {
        //Check enable
        if (!static::$enable)
            return false;

        //Get current uri
        $request = Uri::getUri();

        //Check request matches
        foreach (static::$_routes as $preg => $rout) {
            if (static::compareRouter($preg, $request)) {
                if (isset($rout['module']) && $rout['module'])
                    static::$_router->module = $rout['module'];
                if ($rout['controller'])
                    static::$_router->controller = $rout['controller'];
                else
                    static::$_router->controller = "Index";
                if ($rout['action'])
                    static::$_router->action = $rout['action'];
                else
                    static::$_router->action = "index";

                //Parse and set args
                static::$_router->args = static::parseArg($rout['url']);

                //Result
                return true;
            }
        }
        return false;
    }

    /**
     * Args
     *
     * @var array
     */
    private static $_args;

    /**
     * Compare current request with route
     *
     * @param $router_preg
     * @param $request
     *
     * @return int
     */
    protected static function compareRouter($router_preg, $request)
    {
        $router_preg = trim($router_preg);
        $request = trim(urldecode($request));
        $out = array();
        $x = preg_match("/^$router_preg$/iu", $request, $out);
        for ($i = 1; $i < sizeof($out); $i++) {
            static::$_args[] = $out[$i];
        }
        return $x;
    }

    /**
     * Parse url from route get params in args
     *
     * @param $rewrite_url
     *
     * @return array
     */
    protected static function parseArg($rewrite_url)
    {
        $total = array();
        $out = array();
        $prg = '/\{(\w+)\}/';
        while (preg_match($prg, $rewrite_url, $out)) {
            $total = array_merge($total, array($out[1]));
            $rewrite_url = str_replace($out[1], "", $rewrite_url);
        }
        $args = array();
        for ($i = 0; $i < sizeof($total); $i++) {
            if (isset(static::$_args[$i]))
                $args[$total[$i]] = static::$_args[$i];
            else
                $args[$total[$i]] = null;
        }
        static::$_args = $args;

        return static::$_args;
    }

    /**
     * Get url rewrite
     *
     * @return null|string
     */
    public static function getUrl()
    {
        $key = static::$_router->module . '/' . static::$_router->controller . '/' . static::$_router->action;

        if (isset(static::$_urls[$key]) && $url = static::$_urls[$key]) {
            //Replace params
            foreach (static::$_router->args as $n => $v) {
                $url = str_replace("{" . $n . "}", $v, $url);
            }

            return Url::getAbsoluteUrl() . '/' . $url;
        }

        return NULL;
    }
}