<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Session.php 1 2015-10-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Session
 *
 * @author        Mr.UBKey
 * @link          http://javis.xweb.vn/user_guide/javis_session.html
 */
class Session
{
    const STARTED = true;

    const NOT_STARTED = false;

    /**
     * State of current session
     *
     * @var bool
     */
    private $sessionState = self::NOT_STARTED;

    /**
     * Instance of Class Session
     *
     * @var Session
     */
    private static $instance;

    /**
     * Get current instance of Session
     *
     * @return Session
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        self::$instance->startSession();
        return self::$instance;
    }

    /**
     * (Re)starts session.
     *
     * @return bool
     */
    public function startSession()
    {
        if ($this->sessionState == self::NOT_STARTED) {
            $this->sessionState = session_start();
        }
        return $this->sessionState;
    }

    /**
     * Magic method set session
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * Magic method get session
     *
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
    }

    /**
     * Magic method check session
     *
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($_SESSION[$name]);
    }

    /**
     * Magic method remove session
     *
     * @param $name
     */
    public function __unset($name)
    {
        unset($_SESSION[$name]);
    }

    /**
     * Destroy current session
     *
     * @return bool
     */
    public function destroy()
    {
        if ($this->sessionState == self::STARTED) {
            $this->sessionState = !session_destroy();
            unset($_SESSION);
            return !$this->sessionState;
        }
        return false;
    }
}