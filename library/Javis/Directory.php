<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Directory.php 1 7/23/15 9:00 AM Mr.UBKey $
 */

namespace Javis;


/**
 * Class Directory
 *
 * @package Javis
 * @link            http://javis.xweb.vn/document/javis_directory.html
 */
class Directory
{
    /**
     * Get all sub folder in directory
     * Format: array($folder => $path)
     *
     * @param $dir
     *
     * @return array
     */
    public static function getSubDirectories($dir)
    {
        $arrDir = array();
        $dir .= DIRECTORY_SEPARATOR;
        $ignoredDirectory[] = '.';
        $ignoredDirectory[] = '..';
        if (is_dir($dir)) {
            $dh = opendir($dir);
            if ($dh) {
                while (($folder = readdir($dh)) !== false) {
                    if (!(array_search($folder, $ignoredDirectory) > -1)) {
                        if (filetype($dir . $folder) == "dir")
                            $arrDir[$folder] = $dir . $folder;
                    }
                }
                closedir($dh);
            }
        }
        return $arrDir;
    }

    /**
     * Get all file in directory
     * Format: array($file => $path)
     *
     * @param $directory
     *
     * @return array
     */
    public static function getFiles($directory)
    {
        $arrFile = array();
        $directory .= DIRECTORY_SEPARATOR;
        $ignoredDirectory[] = '.';
        $ignoredDirectory[] = '..';
        if (is_dir($directory)) {
            $dh = opendir($directory);
            if ($dh) {
                while (($file = readdir($dh)) !== false) {
                    if (!(array_search($file, $ignoredDirectory) > -1)) {
                        if (filetype($directory . $file) == "file")
                            $arrFile[array_shift(explode('.', $file))] = $directory . $file;
                    }
                }
                closedir($dh);
            }
        }
        return $arrFile;
    }

    /**
     * filemtime of all file in directory
     *
     * @param $dir
     * @param $regex
     *
     * @return mixed
     */
    public static function getFileMTimeRecursive($dir, $regex = '/^.+$/i')
    {
        if (isset(static::$_fileMTime[$dir . $regex])) {
            return static::$_fileMTime[$dir . $regex];
        }
        $mods = array();
        $directory = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::SELF_FIRST, \RecursiveIteratorIterator::CATCH_GET_CHILD);
        $ri = new \RegexIterator($iterator, $regex, \RecursiveRegexIterator::MATCH);
        foreach ($ri as $path => $dir) {
            $mods[] = filemtime($path);
        }
        arsort($mods);
        return static::$_fileMTime[$dir . $regex] = array_shift($mods);
    }

    private static $_fileMTime = array();
}