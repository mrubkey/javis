<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2013 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: ArrayHelper.php 1 2015-26-08 11:50 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Class ArrayHelper
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_arrayhelper.html
 */
class ArrayHelper
{
    /**
     * Get element in array with .
     * <code>
     *      ArrayHelper::get($array, 'user.name');
     * </code>
     *
     * @param $array
     * @param $key
     * @param null $default
     *
     * @return null
     */
    public static function get($array, $key, $default = null)
    {
        if (is_string($key)) {
            $key = explode('.', $key);
        }

        foreach ($key as $k) {
            if (!is_array($array) || !array_key_exists($k, $array)) {
                return $default;
            }

            $array = $array[$k];
        }

        return $array;
    }

    /**
     * Set value to element in array
     * <code>
     *    ArrayHelper::set($array, 'user.name.first', 'Hello');
     * </code>
     *
     * @param $array
     * @param $key
     * @param $value
     */
    public static function set(&$array, $key, $value)
    {
        if (is_string($key)) {
            $key = explode('.', $key);
        }

        while (count($key) > 1) {
            $k = array_shift($key);
            if (!isset($array[$k]) || !is_array($array[$k])) {
                $array[$k] = array();
            }

            $array =& $array[$k];
        }

        $array[array_shift($key)] = $value;
    }

    /**
     * Delete element in array
     *
     * @param $array
     *
     * @return mixed
     */
    public static function clean(&$array)
    {
        foreach ($array as $key => $ele)
            if (empty($ele))
                unset($array[$key]);
        return $array;
    }

    /**
     * Phương thức chuyển file xml thành mảng
     *
     * @param       $xmlUrl
     * @param array $arrSkipIndices
     *
     * @return array
     */

    /**
     * Convert xml to array
     *
     * @param $xmlPath
     * @param array $arrSkipIndices
     *
     * @return mixed
     */
    public static function xmlToArray($xmlPath, $arrSkipIndices = array())
    {
        return Xml::toArray($xmlPath, $arrSkipIndices);
    }

    /**
     * Secure array
     *
     * @param $array
     *
     * @return mixed
     */
    public static function secure($array)
    {
        for ($i = 0; $i < sizeof($array); $i++) {
            if (is_string($array[$i]))
                $array[$i] = String::secure($array[$i]);
            else
                if (is_array($array[$i]))
                    $array[$i] = self::secure($array[$i]);
        }
        return $array;
    }

    /**
     * Convert array to xml
     *
     * @param $data
     * @param string $rootNodeName
     * @param null $xml
     *
     * @return mixed
     */
    public static function toXml($data, $rootNodeName = 'data', $xml = null)
    {
        //Off mode
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }
        if ($xml == null) {
            $xml = simplexml_load_string(
                "<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }
        foreach ($data as $key => $value) {
            //key is numeric unknownNode_numeric
            if (is_numeric($key)) {
                $key = "unknownNode_" . (string)$key;
            }
            //special character
            $key = preg_replace('/[^a-z]/i', '', $key);
            //recursive
            if (is_array($value)) {
                $node = $xml->addChild($key);
                static::toXml($value, $rootNodeName, $node);
            } else {
                //add new node
                $value = htmlentities($value);
                $xml->addChild($key, $value);
            }
        }
        return $xml->asXML();
    }
}
