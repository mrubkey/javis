<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        XPHP
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2015 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Router.php 1 2015-26-08 15:04 PM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Router
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_router.html
 */
class Router
{
    /**
     * List all modules, controllers, actions
     * @var array
     */
    protected static $applicationTree = array();

    /**
     * Module name
     *
     * @var string
     */
    public $module;

    /**
     * Controller name
     *
     * @var string
     */
    public $controller;

    /**
     * Action name
     *
     * @var string
     */
    public $action;

    /**
     * String args
     *
     * @var array
     */
    public $args = array();

    /**
     * Instance of Class Router
     *
     * @var Router
     */
    private static $_instance;

    /**
     * Init
     */
    protected static function init()
    {
        //Runtime
        Runtime::load('Router', function ($time) {
            return $time >= Directory::getFileMTimeRecursive(APPLICATION_PATH, '/((.*)Controllers$)|((.*)Controllers\/((?!\.).)*$)/i');
        });

        //If runtime empty
        if (!Runtime::get('Router')) {
            static::detectApplicationTree();
        }
    }

    /**
     * Get instance of Router
     *
     * @return Router
     */
    public static function getInstance()
    {
        if (!static::$_instance) {
            static::init();
            static::$_instance = new self();
        }
        return static::$_instance;
    }

    /**
     * Get new instance of Router
     *
     * @param null $router
     *
     * @return Router
     */
    public static function newInstance($router = NULL)
    {
        if (!static::$_instance) {
            static::init();
        }

        $instance = new self();

        if (is_object($router))
            $router = get_object_vars($router);

        //Set router params
        if ($router) {
            if (isset($router['action'])) {
                $instance->action = $router['action'];
            }
            if (isset($router['controller'])) {
                $instance->controller = $router['controller'];
            }
            if (isset($router['module'])) {
                $instance->module = $router['module'];
            }
            if (isset($router['args'])) {
                $instance->args = $router['args'];
            }
        }

        return $instance;
    }

    /**
     * Clone instance
     *
     * @return Router
     */
    public static function cloneInstance()
    {
        return clone static::$_instance;
    }

    /**
     * Parse request uri get Module, Controller, Action, Args
     */
    public function analysisRequestUri()
    {
        Rewrite::init($this);

        $segments = Uri::getSegments();

        //Rewrite processing
        if ($this->isEmpty() && !Rewrite::processing()) {
            //Default
            if (count($segments) == 0) {
                $this->controller = "Index";
                $this->action = "index";
            }

            //Detect module
            if (!$this->module && $this->module = $this->_detectModule(current($segments))) {
                next($segments);
            }

            //Detect controller
            if (!$this->controller && $this->controller = $this->_detectController(current($segments))) {
                next($segments);
            }

            //Detect action
            if (!$this->action && $this->action = $this->_detectAction(current($segments))) {
                next($segments);
            }

            while (false !== ($next = current($segments))) {
                $this->args[$next] = urldecode(next($segments));
                next($segments);
            }

            //Default value
            if ($this->module && !$this->controller)
                $this->controller = "Index";
            if ($this->controller && !$this->action)
                $this->action = "index";
        }
    }

    /**
     * Detect all modules and paths
     *
     * @return array
     */
    protected static function detectApplicationTree()
    {
        //Folder contains
        $dirs = array('' => APPLICATION_PATH);
        $dirs = array_merge($dirs, Directory::getSubDirectories(APPLICATION_PATH . '/' . "Modules"));

        foreach ($dirs as $module => $mPath) {

            //Vlidate module name
            if ($module != '' && (int)preg_match('/^(\w+)$/i', $module) === 0) {
                continue;
            }

            //Detect module and path
            $mIndex = strtolower($module);
            static::$applicationTree[$mIndex] = array('Name' => $module,
                'Map' => $mPath);

            //Get all controllers
            $cFiles = Directory::getFiles($mPath . "/Controllers");
            static::$applicationTree[$mIndex]['Controllers'] = array();

            foreach ($cFiles as $controller => $cPath) {
                //Detect class controller with path
                if (preg_match('/^([a-zA-Z0-9]+)Controller+$/i', $controller, $cMatches)) {
                    $controller = $cMatches[0];
                    $cName = $cMatches[1];
                    $cIndex = strtolower($cName);
                    $class = "$module\\Controllers\\$controller";
                    static::$applicationTree[$mIndex]['Controllers'][$cIndex] = array('Name' => $cName,
                        'Class' => $class,
                        'Map' => $cPath);

                    //Register load namespace
                    Loader::registerNamespaces(array($module => $mPath));

                    //Detect method and get actions
                    require_once $cPath;
                    $reflection = new \ReflectionClass($class);
                    $methods = $reflection->getMethods();
                    $actionList = array(); #Danh sách các action và method của nó
                    foreach ($methods as $m) {
                        $prefix = array('Action', 'Ajax', 'Put', 'Post', 'Get', 'Head', 'Delete', 'Options');
                        if (preg_match('/([a-zA-Z0-9]+)(' . implode('|', $prefix) . '){1}$/i', $m->name, $aMatches)) {
                            $action = $aMatches[1];
                            $aIndex = strtolower($action);
                            $actionList[$aIndex]['Name'] = $action;
                            $actionList[$aIndex]['Methods'][strtolower($aMatches[2])] = $aMatches[2];
                        }
                    }

                    //Save to application tree
                    static::$applicationTree[$mIndex]['Controllers'][$cIndex]['Actions'] = $actionList;
                }
            }
        }

        Runtime::set('Router.Modules', static::$applicationTree);
        Runtime::save('Router');

        return static::$applicationTree;
    }

    /**
     * Check module
     *
     * @param $part
     *
     * @return null
     */
    private function _detectModule($part)
    {
        return Runtime::get("Router.Modules." . strtolower($part) . '.Name');
    }

    /**
     * Check controller
     *
     * @param $part
     *
     * @return null
     */
    private function _detectController($part)
    {
        return Runtime::get("Router.Modules." . strtolower($this->module)
            . ".Controllers." . strtolower($part)
            . ".Name");
    }

    /**
     * Check action
     *
     * @param $part
     *
     * @return null
     */
    private function _detectAction($part)
    {
        return Runtime::get("Router.Modules." . strtolower($this->module)
            . ".Controllers." . strtolower($this->controller)
            . ".Actions." . strtolower($part)
            . ".Name");
    }

    /**
     * Check empty router
     *
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->action)
        && empty($this->controller)
        && empty($this->module);
    }
}