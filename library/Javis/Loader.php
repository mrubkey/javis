<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Loader.php 1 7/24/15 17:00 PM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Loader
 *
 * @package         Javis
 * @link            http://javis.xweb.vn/document/javis_loader.html
 */
class Loader
{
    /**
     * Alias of class
     *
     * @var array
     */
    public static $aliases = array();

    /**
     * Directories
     *
     * @var array
     */
    public static $directories = array();

    /**
     * Mapping class to file
     *
     * @var array
     */
    public static $mappings = array();

    /**
     * Mapping namespace to folder
     *
     * @var array
     */
    public static $namespaces = array();

    /**
     * Register autoload for framework
     * If set $modules = false must load module before use
     * If set $modules = true all module will be loaded and ready to use
     *
     * @param bool|false $modules Autoload all module
     */
    public static function registerAutoload($modules = false)
    {
        //Framework folder autoload
        static::$directories[] = rtrim(LIBRARY_PATH, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        static::$directories[] = rtrim(APPLICATION_PATH, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        //Register autoload
        spl_autoload_register(array('\\Javis\\Loader', 'load'));

        if ($modules) {
            $mods = Directory::getSubDirectories(APPLICATION_PATH . '/Modules');
            static::registerNamespaces($mods);
        }
    }

    /**
     * Load class
     *
     * @param $class
     *
     * @return bool|void
     */
    public static function load($class)
    {
        if (class_exists($class, false) || interface_exists($class, false)) {
            return;
        }

        //Check class alias
        if (isset(static::$aliases[$class])) {
            return class_alias(static::$aliases[$class], $class);
        } //Check mapping class
        else if (isset(static::$mappings[$class])) {
            require static::$mappings[$class];
            return;
        }

        //Check class with namespace
        foreach (static::$namespaces as $namespace => $directory) {
            if (strpos($class, $namespace) === 0) {
                static::loadNamespace($class, $namespace, $directory);
                return;
            }
        }

        static::loadClass($class);
    }

    /**
     * Load class with directory
     *
     * @param $class
     * @param null $directory
     *
     * @return mixed|void
     */
    public static function loadClass($class, $directory = NULL)
    {
        // Convert _ to \ in class name
        $file = str_replace(array('\\', '_'), DIRECTORY_SEPARATOR, $class);

        if (is_file($p = $file . '.php')) {
            return require $p;
        } else {
            $directories = static::$directories;

            //Use directory if use
            if ($directory !== NULL) {
                if (is_array($directory)) {
                    $directories = static::format($directory);
                } else {
                    $directory = rtrim($directory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
                    if (is_file($p = $directory . $file . '.php')) {
                        return require $p;
                    } else if (is_file($p = $directory . strtolower($file) . '.php')) {
                        return require $p;
                    }
                }
            }

            //Check all files in all directories
            foreach ((array)$directories as $d) {
                if (is_file($p = $d . $file . '.php')) {
                    return require $p;
                } else if (is_file($p = $d . strtolower($file) . '.php')) {
                    return require $p;
                }
            }
        }

        return;
    }

    /**
     * Load file
     *
     * @param $file
     *
     * @param null $directory
     *
     * @return bool
     */
    public static function loadFile($file, $directory = null)
    {
        if ($directory !== NULL) {
            if (is_array($directory)) {
                $directory = static::format($directory);
                foreach ($directory as $d) {
                    if (is_file($p = $d . strtolower($file) . '.php')) {
                        require $p;
                    } else if (is_file($p = $d . $file . '.php')) {
                        require $p;
                    }
                }
            } else {
                $directory = rtrim($directory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
                if (is_file($p = $directory . strtolower($file) . '.php')) {
                    require $p;
                } else if (is_file($p = $directory . $file . '.php')) {
                    require $p;
                }
            }
        }

        return;
    }

    /**
     * Load class with namespace
     *
     * @param $class
     * @param $namespace
     * @param $directory
     *
     * @return mixed|void
     */
    protected static function loadNamespace($class, $namespace, $directory)
    {
        return static::loadClass(substr($class, strlen($namespace)), $directory);
    }

    /**
     * Format directories path
     *
     * @param $directories
     *
     * @return array
     */
    protected static function format($directories)
    {
        $callback = function ($directory) {
            return rtrim($directory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        };
        return array_map($callback, (array)$directories);
    }

    /**
     * Add array mapping class to file
     *
     * @param array $mappings
     */
    public static function registerMaps(array $mappings)
    {
        static::$mappings = array_merge(static::$mappings, $mappings);
    }

    /**
     * Add mapping class to file
     *
     * @param $class
     * @param $mapping
     */
    public static function registerMap($class, $mapping)
    {
        static::$mappings[$class] = $mapping;
    }

    /**
     * Add alias of class
     *
     * @param $class
     * @param $alias
     */
    public static function registerAlias($class, $alias)
    {
        static::$aliases[$alias] = $class;
    }

    /**
     * Add mapping namespace to folder
     *
     * @param $mappings
     * @param string $append
     */
    public static function registerNamespaces($mappings, $append = '\\')
    {
        $namespaces = array();

        foreach ($mappings as $namespace => $directory) {
            //Separator namespace
            $namespace = trim($namespace, $append) . $append;
            //Remove old mapping
            unset(static::$namespaces[$namespace]);
            $namespaces[$namespace] = reset(static::format($directory));
        }

        static::$namespaces = array_merge($namespaces, static::$namespaces);
    }

    /**
     * Add directory
     *
     * @param $directory
     */
    public static function registerDirectories($directory)
    {
        $directories = static::format($directory);

        static::$directories = array_unique(array_merge(static::$directories, $directories));
    }
}