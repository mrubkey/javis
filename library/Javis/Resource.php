<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Resource.php 1 2015-22-03 02:05:09 PM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Resource
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_resource.html
 */
class Resource
{
    /**
     * Path to resource folder
     *
     * @var string
     */
    private static $resourcePath = 'resources';

    /**
     * Array contains resources
     *
     * @var array
     */
    private static $resourceArray = array();

    /**
     * Language in used
     *
     * @var string
     */
    public $language;

    /**
     * Resource name
     *
     * @var string
     */
    public $name;

    /**
     * Resource name default
     *
     * @var string
     */
    public $default;

    /**
     * Construct
     */
    public function __construct()
    {
    }

    /**
     * Set default resource
     */
    public function setDefault()
    {
        Runtime::setFlash("DefaultResource", $this);
    }

    /**
     * Check resource exists in loaded
     *
     * @param null $name
     *
     * @return bool
     */
    public function hasResource($name = NULL)
    {
        if ($name === NULL)
            return sizeof(self::$resourceArray) > 0;
        else
            return isset(self::$resourceArray[$name]);
    }

    /**
     * Get resource language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set language for resources
     *
     * @param $lang
     */
    public function setLanguage($lang)
    {
        $this->language = $lang;
    }

    /**
     * Get string from resource
     *
     * @param $name
     *
     * @return string
     */
    public function getString($name)
    {
        //Get resource array
        if ($this->language)
            $resource = self::$resourceArray[$this->name][$this->language];
        else
            $resource = self::$resourceArray[$this->name]['default'];

        /*
         * Get resource use >
         */
        $arrKey = explode('>', $name);
        if (sizeof($arrKey) == 1) {
            if ($this->hasPhrase($name))
                $str = $resource[$name];
        } else {
            $pNode = array();
            for ($i = 0; $i < sizeof($arrKey); $i++) {
                //Trim
                $arrKey[$i] = trim($arrKey[$i]);
                //Root node
                if ($i == 0)
                    $pNode = $resource[$name];
                else {
                    if (isset($pNode[$arrKey[$i]]))
                        $pNode = $pNode[$arrKey[$i]];
                }
            }
            $str = $pNode;
        }
        return isset($str['#text']) ? $str['#text'] : "";
    }

    public function __get($name)
    {
        return $this->getString($name);
    }

    /**
     * Check resource contains string
     *
     * @param $name
     *
     * @return bool
     */
    public function hasString($name)
    {
        $str = $this->getString($name);
        return !empty($str);
    }

    /**
     * Check phrase contains in resource
     *
     * @param $name
     *
     * @return bool
     */
    public function hasPhrase($name)
    {
        if ($this->language)
            $resource = self::$resourceArray[$this->name][$this->language];
        else
            $resource = self::$resourceArray[$this->name]['default'];
        if (isset($resource[$name]))
            return true;
        else
            return false;
    }

    /**
     * Instance of resource
     *
     * @var Resource
     */
    private static $instance;

    /**
     * Get instance of Resource
     *
     * @return Resource
     */
    public static function getInstance()
    {
        if (static::$instance)
            return static::$instance;
        else
            return static::$instance = new self();
    }

    /**
     * Load resource
     *
     * @param $name
     * @param null $resource
     *
     * @return null|Resource
     */
    public static function load($name, $resource = NULL)
    {
        $arrXmlFile = array();
        $handle = opendir(self::$resourcePath);
        if ($handle) {
            /*
			 * load file xml with name
			 */
            while (false !== ($file = readdir($handle))) {
                if (String::startsWith($file, $name, false) && String::endsWith($file, ".xml", false)) {
                    $arrXmlFile[] = $file;
                }
            }
        }
        //set content to resource instance
        if (!$resource) {
            $resource = self::getInstance();
            $resource->name = $name;
        }
        foreach ($arrXmlFile as $file) {
            //parse file name get language code
            $filePart = explode(".", $file);
            //load file with DOM format
            if (sizeof($filePart) == 3) {
                //check resource in runtime
                $rPath = static::$resourcePath;
                Runtime::load("Resource", function ($time) use ($rPath) {
                    return $time >= filemtime($rPath);
                });
                $runtimeKey = "Resource.{$filePart[0]}.{$filePart[1]}";
                if (!$data = Runtime::get($runtimeKey)) {
                    $xml = new Xml(self::$resourcePath . DIRECTORY_SEPARATOR . $file);
                    //set resource to array
                    self::$resourceArray[$filePart[0]][$filePart[1]] = $xml->getArray();
                    //save runtime
                    Runtime::set($runtimeKey, $xml->getArray());
                    Runtime::save("Resource");
                } else
                    self::$resourceArray[$filePart[0]][$filePart[1]] = $data;
            } else
                if (sizeof($filePart) == 2) {
                    //check resource in runtime
                    $runtimeKey = "Resource.{$filePart[0]}";
                    if (!$data = Runtime::get($runtimeKey)) {
                        $dom = new Xml(self::$resourcePath . DIRECTORY_SEPARATOR . $file);
                        //set resource to array
                        self::$resourceArray[$filePart[0]]['default'] = $dom->getArray();
                        //save runtime
                        Runtime::set($runtimeKey, $dom->getArray());
                        Runtime::save("Resource");
                    } else
                        self::$resourceArray[$filePart[0]]['default'] = $data;
                }
        }
        //return instance
        return $resource;
    }

    /**
     * Load from config
     *
     * @param $name
     *
     * @return null|Resource
     */
    public static function loadConfig($name)
    {
        //get instance
        $resource = static::getInstance();
        //load resources in config
        $config = Config::get("Resources");
        $name = $config->name;
        if (isset($config->default) && $config->default) {
        }
        //load resource
        return self::load($name, $resource);
    }
}