<?php
/**
 * XPHP Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category        XPHP
 * @package         XPHP\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://xphp.xweb.vn/license.html     GNU GPL License
 * @version         $Id: XCache.php 201012 2011-22-08 02:05:09 Mr.UBKey $
 */

namespace XPHP\Cache;

/**
 * XCache
 *
 * @category    XPHP
 * @package     XPHP\Cache
 * @author      Mr.UBKey
 * @link        http://xphp.xweb.vn/user_guide/xphp_cache_xcache.html
 */
class XCache extends CacheAbstract
{
    public function __construct()
    {
        //Load cấu hình trong file config với node cache > xcache
        //require_once 'XPHP/Config.php';
        //XPHP_Config::load($this, 'cache > xcache');
    }

    /**
     * __set Gán giá trị cache
     *
     * @access public
     *
     * @param mixed $name
     * @param mixed $value
     *
     * @return void
     */
    public function __set($name, $value)
    {
        xcache_set($name, $value, $this->time);
    }

    /**
     * Gán giá trị cache
     *
     * @access public
     *
     * @param mixed $name
     * @param mixed $value
     *
     * @return \XPHP\Cache\XCache
     */
    public function set($name, $value, $time = NULL)
    {
        xcache_set($name, $value, $time);

        return $this;
    }


    /**
     * __get Lấy giá trị cache
     *
     * @access public
     *
     * @param mixed $name
     *
     * @return void
     */
    public function __get($name)
    {
        return xcache_get($name);
    }

    /**
     * Lấy giá trị cache
     *
     * @access public
     *
     * @param mixed $name
     *
     * @return void
     */
    public function get($name)
    {
        return xcache_get($name);
    }

    /**
     * __isset Kiểm tra sự tồn tại của cache
     *
     * @access public
     *
     * @param mixed $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return xcache_isset($name);
    }

    /**
     * __unset Xoá bỏ giá trị
     *
     * @access public
     *
     * @param mixed $name
     *
     * @return void
     */
    public function __unset($name)
    {
        xcache_unset($name);
    }
}