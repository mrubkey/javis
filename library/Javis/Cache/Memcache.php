<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Memcache.php 1 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis\Cache;

defined('MEMCACHE_HOST') || define('MEMCACHE_HOST', 'localhost');

defined('MEMCACHE_PORT') || define('MEMCACHE_PORT', 11211);

/**
 * Class Memcache
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/document/javis_cache_memcache.html
 */
class Memcache extends CacheAbstract
{
    /**
     * @var \Memcache
     */
    private static $_memcache;

    public static function connect()
    {
        if (!static::$_memcache) {
            static::$_memcache = new \Memcache();
            if (!@ static::$_memcache->connect(MEMCACHE_HOST, MEMCACHE_PORT))
                static::$_memcache = NULL;
        }
        return static::$_memcache ? true : false;
    }

    /**
     * Set cache magically
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        static::$_memcache->set($name, $value, $this->compresses, $this->time);
    }

    /**
     * Set cache
     *
     * @param $name
     * @param $value
     * @param null $time
     *
     * @return $this
     */
    public function set($name, $value, $time = NULL)
    {
        static::$_memcache->set($name, $value, $this->compresses, $time);

        return $this;
    }

    /**
     * Get cache value magically
     *
     * @param $name
     *
     * @return array|string
     */
    public function __get($name)
    {
        return static::$_memcache->get($name);
    }

    /**
     * Get cache value
     *
     * @param $name
     *
     * @return array|string
     */
    public function get($name)
    {
        return static::$_memcache->get($name);
    }

    /**
     * Check exist
     *
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return static::$_memcache->get($name) !== false;
    }

    /**
     * Delete cache
     *
     * @param $name
     */
    public function __unset($name)
    {
        static::$_memcache->delete($name);
    }
}