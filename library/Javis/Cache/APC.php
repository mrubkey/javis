<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: APC.php 1 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis\Cache;

/**
 * APC
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_cache_apc.html
 */
class APC extends CacheAbstract
{
    public function __construct()
    {
    }

    /**
     * Set value to cache magically
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (apc_exists($name))
            apc_store($name, $value, $this->time);
        else
            apc_add($name, $value, $this->time);
    }

    /**
     * Set value to apc cache
     *
     * @param $name
     * @param $value
     * @param null $time
     *
     * @return $this
     */
    public function set($name, $value, $time = NULL)
    {
        if (apc_exists($name))
            apc_store($name, $value, $time);
        else
            apc_add($name, $value, $time);

        return $this;
    }

    /**
     * Get value from cache magically
     *
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return apc_fetch($name);
    }

    /**
     * Get value from apc cache
     *
     * @param $name
     *
     * @return mixed
     */
    public function get($name)
    {
        return apc_fetch($name);
    }

    /**
     * Check exist cache magically
     *
     * @param $name
     *
     * @return bool|\string[]
     */
    public function __isset($name)
    {
        return apc_exists($name);
    }

    /**
     * Delete cache magically
     *
     * @param $name
     */
    public function __unset($name)
    {
        apc_delete($name);
    }
}