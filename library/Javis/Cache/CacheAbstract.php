<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2015 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: CacheAbstract.php 201012 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis\Cache;

/**
 * Cache Abstract
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_cache_abstract.html
 */
abstract class CacheAbstract
{
    /**
     * Path to folder cache
     *
     * @var string
     */
    public $path;

    /**
     * Prefix of cache key
     *
     * @var string
     */
    public $prefix;

    /**
     * Cache time (s)
     *
     * @var int
     */
    public $time = 43200;

    /**
     * Use compressed data
     *
     * @var bool
     */
    public $compresses;

    /**
     * Get cache time
     *
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set cache time
     *
     * @param $time
     *
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get path to cache
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set cache path
     *
     * @param $path
     *
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get cache prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set cache prefix
     *
     * @param $prefix
     *
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Set compressed data to cache
     *
     * @param $bool
     *
     * @return $this
     */
    public function compresses($bool)
    {
        $this->compresses = $bool;

        return $this;
    }

    public abstract function get($key);

    public abstract function set($key, $value, $time);
}