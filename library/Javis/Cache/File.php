<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: File.php 1 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis\Cache;

/**
 * File
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/document/javis_cache_file.html
 */
class File extends CacheAbstract
{
    public function __construct($time = NULL, $compresses = NULL, $prefix = NULL, $path = NULL)
    {
        if ($path !== NULL)
            $this->path = $path;
        else if (!$this->path)
            $this->path = STORAGE_PATH . "/Cache/Files";

        if(! is_dir($this->path))
            mkdir($this->path, 0777, true);

        if ($time !== NULL)
            $this->time = $time;
        else
            $this->time = 43200;

        if ($prefix !== NULL)
            $this->prefix = $prefix;

        if ($compresses !== NULL)
            $this->compresses = $compresses;
        else if (!$this->compresses)
            $this->compresses = false;
    }

    /**
     * Delete cache
     *
     * @param $key
     */
    public function __unset($key)
    {
        $filename_cache = $this->getCacheFileName($key); //file name
        $filename_info = $this->getInfoFileName($key); //file info name
        //Xóa file cache và file info
        if (is_file($filename_cache))
            unlink($filename_cache);
        if (is_file($filename_info))
            unlink($filename_info);
    }

    /**
     * Check exist cache
     *
     * @param $key
     *
     * @return bool
     */
    public function __isset($key)
    {
        $filename_cache = $this->getCacheFileName($key); //file name
        $filename_info = $this->getInfoFileName($key); //file info name

        if (file_exists($filename_cache) && file_exists($filename_info)) {
            $cache_time = file_get_contents($filename_info) + (int)$this->getTime(); //Last update of cache
            $time = time(); //current time

            $expiry_time = (int)$time; //expire time

            if ((int)$cache_time >= (int)$expiry_time) //compare expire time
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Get value magically
     *
     * @param $key
     *
     * @return null|string
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Get value
     *
     * @param $key
     *
     * @return null|string
     */
    public function get($key)
    {
        $filename_cache = $this->getCacheFileName($key);
        $filename_info = $this->getInfoFileName($key);

        if (file_exists($filename_cache) && file_exists($filename_info)) {
            $expire_time = (int)file_get_contents($filename_info) + (int)$this->getTime();
            $time = time();

            if ((int)$expire_time >= $time)
            {
                return file_get_contents($filename_cache); //Get cache content
            }
        }

        return NULL;
    }

    /**
     * Set cache magically
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        $time = time() + $this->time;

        if (!file_exists($this->getPath()))
            mkdir($this->getPath());

        $filename_cache = $this->getCacheFileName($key);
        $filename_info = $this->getInfoFileName($key);

        file_put_contents($filename_cache, $value);
        file_put_contents($filename_info, $time);
    }

    /**
     * Set cache
     *
     * @param $key
     * @param $value
     * @param null $time
     *
     * @return $this
     */
    public function set($key, $value, $time = NULL)
    {
        if ($time === NULL) {
            $time = $this->time;
        }

        $time = time() + $time;

        if (!file_exists($this->getPath()))
            mkdir($this->getPath());

        $filename_cache = $this->getCacheFileName($key);
        $filename_info = $this->getInfoFileName($key);

        file_put_contents($filename_cache, serialize($value));
        file_put_contents($filename_info, $time);

        return $this;
    }

    /**
     * Get name of file cache
     *
     * @param $key
     *
     * @return string
     */
    public function getCacheFileName($key)
    {
        return $this->path . DIRECTORY_SEPARATOR . $this->getPrefix() . md5($key) . '.cache';
    }

    /**
     * Get name of file info
     *
     * @param $key
     *
     * @return string
     */
    public function getInfoFileName($key)
    {
        return $this->path . DIRECTORY_SEPARATOR . $this->getPrefix() . md5($key) . '.info';
    }
}