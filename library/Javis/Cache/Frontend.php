<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Frontend.php 1 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis\Cache;

/**
 * Class Frontend
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/document/javis_cache_file.html
 */
class Frontend extends CacheAbstract
{
    /**
     * Path to cache file
     *
     * @var string
     */
	private $file;
	
	public function __construct($time=NULL, $compresses=NULL, $prefix=NULL, $path=NULL)
    {
		if($path !== NULL)
			$this->path = $path;
		else if(!$this->path)
			$this->path = STORAGE_PATH . "/Cache/Frontend";

        if(! is_dir($this->path))
            mkdir($this->path, 0777, true);

		if($time !== NULL)
			$this->time = $time;
			
		if ($prefix !== NULL)
			$this->prefix = $prefix;
		else if(!$this->prefix)
			$this->prefix = "xcf_";
			
		if ($compresses !== NULL)
			$this->compresses = $compresses;
		else if(!$this->compresses)
			$this->compresses = false;
    }

    /**
     * Open cache block
     *
     * @param $cache_id
     * @param null $expire
     *
     * @return bool
     */
    public function begin($cache_id, $expire = NULL)
    {
        //check end cache section
        if ($this->file)
            trigger_error('Must end() before begin() new cache section.', E_USER_ERROR);

        $this->file = $this->getFileName($cache_id);

        if ($expire)
            $this->setTime($expire);

        //if has cache file
        if (is_readable($this->file)) {
            if (!$this->cacheExpired()) {
                //show content from cache file
                echo file_get_contents($this->file);

                return false;
            }
        }

        ob_start();

        return true;
    }

    /**
     * End of cache section
     */
    public function end() 
    {    
        //check begin cache section
        if (!$this->file)
        	trigger_error('Must begin() before end().' , E_USER_ERROR);

        //create cache file
        if ($this->cacheExpired()) { 
            $file = fopen($this->file, 'c'); 
            if (flock($file, LOCK_EX)) { 
                ftruncate($file, 0); 
                fwrite($file, ob_get_flush()); 
            } else { 
                ob_flush(); 
            } 
            fclose($file); 
        } 
         
        //delete file to begin new cache section
        $this->file = null; 
    }

    /**
     * Check expire cache
     *
     * @return bool
     */
    private function cacheExpired() 
    {
    	if(is_file($this->file))
	        return time() - filemtime($this->file) > $this->getTime();
	    else
	    	return true;
    }

    /**
     * Get file name current cache section
     *
     * @param $cache_id
     *
     * @return string
     */
    public function getFileName($cache_id)
    {
    	return $this->getPath() . DIRECTORY_SEPARATOR . $this->getPrefix() . md5($cache_id);
    }

    /**
     * Delete file cache
     *
     * @param $cache_id
     */
    public function remove($cache_id)
    {
    	unlink($this->getFileName($cache_id));
    }

    public function get($key)
    {}

    public function set($key, $value=NULL, $time=NULL)
    {
        $this->begin($key, $time);
    }
} 