<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package     Javis\Cache
 * @author        XWEB Dev Team
 * @copyright    Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Output.php 1 2015-25-07 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Cache;

use Javis\Router;
use Javis\Uri;

/**
 * Class Output Cache
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/document/javis_cache_output.html
 */
class Output extends CacheAbstract
{
    /**
     * @var Output
     */
    public static $instance;

    /**
     * Router of current request
     *
     * @var Router
     */
    private $_router;

    /**
     * Call display() to run output cache
     * Open output buffering and create file cache
     * ob_start('$this->cache');
     *
     * @param null $time
     * @param null $compresses
     * @param null $prefix
     * @param null $path
     */
    public function __construct($time = NULL, $compresses = NULL, $prefix = NULL, $path = NULL)
    {
        //Cache path
        if ($path !== NULL)
            $this->path = $path;
        else if (!$this->path)
            $this->path = STORAGE_PATH . "/Cache/Outputs";

        //Check path and validate dir
        if (!is_dir($this->path)) {
            mkdir(STORAGE_PATH . "/Cache/Outputs", 0777, true);
        }

        if ($time !== NULL)
            $this->time = $time;

        if ($prefix !== NULL)
            $this->prefix = $prefix;
        else if (!$this->prefix)
            $this->prefix = "xco_";

        if ($compresses !== NULL)
            $this->compresses = $compresses;
        else if (!$this->compresses)
            $this->compresses = false;
    }

    /**
     * Get cache file name
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->getPath() . DIRECTORY_SEPARATOR . $this->getPrefix() . $this->getUri();
    }

    /**
     * Display content from cache file when cache file is valid
     *
     * @param bool|true $exit
     *
     * @return bool
     */
    public function displayCache($exit = true)
    {
        $file = $this->getFileName();

        //Check exists
        if (!file_exists($file)) return false;

        //Check time
        if (filemtime($file) < time() - $this->getTime())
            return false;

        //Show content cache file
        echo file_get_contents($file);

        //Stop process
        if ($exit)
            exit;
        else
            return true;
    }

    /**
     * Write cache content to file
     *
     * @param $content
     *
     * @return mixed
     */
    public function startCache($content)
    {
        $file = $this->getFileName();
        if (false !== ($f = @fopen($file, 'w'))) {
            fwrite($f, $content);
            fclose($f);
        }
        return $content;
    }

    /**
     * Call cache
     *
     * @param $content
     *
     * @return mixed
     */
    public static function cache($content)
    {
        //Change dir to root dir when ob_start call callback
        chdir(dirname($_SERVER['SCRIPT_FILENAME']));
        //Get instance
        $ocInstance = static::getInstance();
        //Get file name
        $file = $ocInstance->getFileName();
        //Open and write output to cache
        if (false !== ($f = @fopen($file, 'w'))) {
            @fwrite($f, $content);
            @fclose($f);
        }
        //Return content to buffer
        return $content;
    }

    /**
     * Remove cache
     *
     * @param string $path
     */
    public function remove($path = null)
    {
        $file = $this->getFileName() . md5($path);
        if (file_exists($file)) unlink($file) or die("Không thể xóa file cache: $file!");
    }

    /**
     * Set current Router
     *
     * @param $router
     */
    public function setRouter($router)
    {
        $this->_router = $router;
    }

    /**
     * Get uri
     *
     * @return mixed|string
     */
    public function getUri()
    {
        if ($this->_router) {
            $uri = "";
            if ($this->_router->module)
                $uri = $this->_router->module . "/";
            $uri .= $this->_router->controller . "/";
            $uri .= $this->_router->action;
            //Get all args
            $uri .= implode("/", $this->_router->args);
        } else
            $uri = Uri::getRequestUri();

        //Remove all symbols
        $uri = str_replace(array('?', '&'), '_', trim($uri, '/'));

        return $uri;
    }

    /**
     * Get instance of Ouput Cache
     *
     * @param null $time
     * @param null $compresses
     * @param null $prefix
     * @param null $path
     *
     * @return Output
     */
    public static function getInstance($time = NULL, $compresses = NULL, $prefix = NULL, $path = NULL)
    {
        if (self::$instance != null)
            return self::$instance;
        else {
            self::$instance = new self($time = NULL, $compresses = NULL, $prefix = NULL, $path = NULL);
            return self::$instance;
        }

    }

    /**
     * Set instance of Output Cache
     *
     * @param Output $instance
     */
    public static function setInstance(Output $instance)
    {
        self::$instance = $instance;
    }

    public function set($key, $value, $time = NULL)
    {
    }

    public function get($key)
    {
    }
}