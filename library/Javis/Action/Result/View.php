<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category        XPHP
 * @package         XPHP\Action
 * @subpackage      XPHP\Action\Result
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: View.php 1 2015-02-02 PM 16:08:09 Mr.UBKey $
 */

namespace Javis\Action\Result;

use Javis\Asset;
use Javis\Cache\Frontend;
use Javis\Config;
use Javis\Cookie;
use Javis\Form;
use Javis\Html;
use Javis\Layout;
use Javis\Resource;
use Javis\Router;
use Javis\Runtime;
use Javis\Session;
use Javis\Url;
use Javis\View\Data;
use Javis\View\Layout\Content;
use Javis\View\Layout\Section;
use Javis\Widget;

/**
 * Class View
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_action_result_view.html
 */
class View extends ResultAbstract
{
    /**
     * @var Html
     */
    protected $html;

    /**
     * @var Form
     */
    protected $form;

    /**
     * @var Url
     */
    protected $url;

    /**
     * Lớp hỗ trợ cache frontend
     *
     * @var Frontend
     */
    protected $cache;

    /**
     * @var Section
     */
    protected $section;

    /**
     * @var Content
     */
    protected $content;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Cookie
     */
    protected $cookie;

    /**
     * @var Resource
     */
    protected $resource;

    /**
     * @var Asset
     */
    protected $asset;

    /**
     * @var Widget
     */
    protected $widget;


    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Layout
     */
    private $_layout;

    /**
     * Tên file layout
     *
     * @var string
     */
    protected $layout;

    /**
     * Dữ liệu chuyển từ controller sang view
     *
     * @var \stdClass
     */
    protected $viewData;

    /**
     * @var \Javis\View
     */
    protected $view;

    /**
     * Khởi tạo
     *
     * @param null $name Tên view cần render nếu NULL lấy view mặc định
     */
    public function __construct($name = NULL)
    {
        //Khởi tạo view
        $this->view = new \Javis\View($this->router, $name);
        //Khởi tạo layout
        $this->_layout = new Layout($this->router);

        //Load các biến trong file config với node view > viewData
        $views = Config::get('View');
        $this->viewData = new Data();
        if ($views) {
            $this->setViewData($views);
        }
    }

    /**
     * Set router
     *
     * @param $router
     */
    public function setRouter(&$router)
    {
        $this->router = $router;
    }

    /**
     * Set view data from action
     *
     * @param $data
     * @param bool|false $override
     */
    public function setViewData($data, $override = false)
    {
        if ($override)
            $this->viewData = $data;
        else {
            foreach ($data as $key => $value) {
                $this->viewData->$key = $value;
            }
        }
    }

    /**
     * Set layout
     *
     * @param $layout
     */
    public function setLayoutFile($layout)
    {
        $this->layout = $layout;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \XPHP\Action\Result\Interface::execute()
     */
    public function execute()
    {
        extract(get_object_vars($this->viewData), EXTR_OVERWRITE);

        //Session
        $this->session = Session::getInstance();

        //Cookie
        $this->cookie = Cookie::getInstance();

        //HTML
        $this->html = new Html($this->router, $this);

        //Form
        $this->form = new Form($this->router, $this);

        //URL
        $this->url = new Url($this->router);

        //Asset
        $this->asset = new Asset($this->url);

        //Xử lý resource
        if (Runtime::hasFlash("DefaultResource"))
            $this->resource = Runtime::get("DefaultResource");

        //Section Layout
        $this->section = new Section();

        //Content Layout
        $this->content = new Content();

        //Cache frontend
        $this->cache = new Frontend();

        //Widget
        $this->widget = new Widget();

        //Check has layout process load file Layout and get content from View
        if ($this->layout !== null) {
            ob_start();
            require $this->view->getViewPath();
            $content = ob_get_contents();
            ob_end_clean();
            if (!(string)$this->content && !empty($content)) {
                $this->content->set($content);
            }
            //set layout
            $this->_layout->setLayoutFile($this->layout);
            //inclide layout
            require $this->_layout->getLayoutPath();
        } else {
            //include file View
            require $this->view->getViewPath();
        }
    }
}