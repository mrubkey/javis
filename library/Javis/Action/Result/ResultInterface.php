<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis\Action\Result
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: ResultInterface.php 1 2015-22-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Action\Result;

/**
 * Interface ResultInterface
 *
 * @author    Mr.UBKey
 */
interface ResultInterface
{
    /**
     * Method execute Result
     *
     * @return mixed
     */
    public function execute();
}