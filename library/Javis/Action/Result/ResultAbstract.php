<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis\Action\Result
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: ResultAbstract.php 1 2015-02-07 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Action\Result;

use Javis\Model;

/**
 * Class ResultAbstract
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_action_result_resultabstract.html
 */
abstract class ResultAbstract implements ResultInterface
{
    /**
     * Model
     *
     * @var Model
     */
    public $model;

    /**
     * Get Model
     *
     * @return Model
     */
    public function getModel()
    {
        if (!empty($this->model)) {
            return $this->model;
        }
    }

    /**
     * Set Model for Result
     *
     * @param $model
     */
    public function setModel(&$model)
    {
        $this->model = $model;
    }

    public function setRouter()
    {
    }

    public function setLayoutFile()
    {
    }

    public function setViewData()
    {
    }
}