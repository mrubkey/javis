<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis\Action\Result
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Redirect.php 20116 2011-22-08 02:05:09 Mr.UBKey $
 */

namespace Javis\Action\Result;

use Javis\Router;
use Javis\Url;


/**
 * Class Redirect
 *
 * @author    Mr.UBKey
 * @link    http://javis.xweb.vn/document/javis_action_result_redirect.html
 */
class Redirect implements ResultInterface
{
	/**
	 * Url
	 *
	 * @var string
	 */
	public $url;

	public function __construct($url)
	{
		if ($url instanceof Router) {
			$this->url = Url::getActionUrl($url);
		}
		else if (is_string($url)) {
			$this->url = $url;
		}
		else {
			throw new \Exception("Params Javis\\Action\\Result\\Redirect is string or instance of Javis\\Router");
		}
	}

	public function execute()
	{
		Url::redirectUrl($this->url);
	}
}