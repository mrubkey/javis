<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package     Javis\Action\Annotation
 * @author		XWEB Dev Team
 * @copyright	Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license		http://javis.xweb.vn/license.html     GNU GPL License
 * @version		$Id: Compresses.php 1 2015-20-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Action\Annotation;

use Javis\Annotation\AnnotationAbstract;

/**
 * Class Compresses
 *
 * @author 		Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_action_annotation_compresses.html
 * @Annotation
 * @Target("METHOD")
 */
class Compresses extends AnnotationAbstract
{
    /**
     * Override to handle before action execute
     */
    public function onActionExecute ()
    {
        if (! ob_start("ob_gzhandler"))
            ob_start();
    }
}