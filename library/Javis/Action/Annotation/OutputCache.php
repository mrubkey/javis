<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package     Javis\Action\Annotation
 * @author		XWEB Dev Team
 * @copyright	Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license		http://javis.xweb.vn/license.html     GNU GPL License
 * @version		$Id: OutputCache.php 1 2015-02-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Action\Annotation;

use Javis\Cache\Output;

/**
 * OutputCache
 *
 * @author		Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_action_annotation_outputcache.html
 * @Annotation
 * @Target("METHOD")
 */
class OutputCache extends AnnotationAbstract
{
    /**
     * Cache time
     *
     * @var string
     */
    public $time;

    /**
     * Compresses data
     *
     * @var bool
     */
    public $compresses;

    /**
     * Cache prefix
     *
     * @var string
     */
    public $prefix;

    /**
     * Path to cache folder
     *
     * @var string
     */
    public $path;

    /**
     * Set params for annotation
     *
     * @param $properties
     */
    public function init ($properties)
    {
        if (isset($properties['time']))
            $this->time = $properties['time'];
        if (isset($properties['compresses']))
            $this->compresses = $properties['compresses'];
        if (isset($properties['prefix']))
            $this->prefix = $properties['prefix'];
        if (isset($properties['path']))
            $this->path = $properties['path'];
    }

    /**
     * Override to fire before action execute
     */
    public function onActionExecute ()
    {
        //Create instance output cache
        $cacheOutput = Output::getInstance($this->time,
                                           $this->compresses,
                                           $this->prefix,
                                           $this->path);
        //Set current router to get uri
        $cacheOutput->setRouter($this->controller->getRouter());
        //Stop process when show cache
        if($cacheOutput->displayCache(false))
        {
            //free controller
            $this->freeController();
            exit();
        }
        //Open output buffering and create file cache
        ob_start("\\Javis\\Cache\\Output::cache");
    }
}