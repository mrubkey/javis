<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package		Javis\Action\Annotation
 * @author		XWEB Dev Team
 * @copyright	Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license		http://javis.xweb.vn/license.html     GNU GPL License
 * @version		$Id: AnnotationAbstract.php 1 2015-01-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Action\Annotation;

use Javis\Controller;

/**
 * Class AnnotationAbstract
 *
 * @author      Mr.UBKey
 */
class AnnotationAbstract extends \Javis\Annotation\AnnotationAbstract
{
    /**
     * Event fire before action execute
     */
    public function onActionExecute ()
    {}

    /**
     * Event fire after action execute
     *
     * @param $result
     */
    public function onActionExecuted ($result)
    {}

    /**
     * @var Controller
     */
    protected $controller;

    /**
     * Set Controller
     *
     * @param Controller $controller
     */
    public function setController (&$controller)
    {
        $this->controller = $controller;
    }

    /**
     * Free Controller
     */
    public function freeController()
    {
        $this->controller = null;
    }
}