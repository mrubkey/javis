<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        XPHP
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Cookie.php 1 2015-10-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Cookie
 *
 * @author        Mr.UBKey
 * @link          http://javis.xweb.vn/user_guide/javis_cookie.html
 */
class Cookie
{
    /**
     * Cookie expire
     *
     * @var int
     */
    public $expire;

    /**
     * Instance of Cookie
     *
     * @var Cookie
     */
    private static $instance;

    /**
     * Get current instance of Cookie
     *
     * @return Cookie
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Set cookie expire
     *
     * @param $time
     *
     * @return $this
     */
    public function expire($time)
    {
        $this->expire = $time;
        return $this;
    }

    /**
     * Set cookie
     *
     * @param $name
     * @param $value
     * @param null $expire
     *
     * @return Cookie
     */
    public function set($name, $value, $expire=NULL)
    {
        $_COOKIE[$name] = $value;
        if ($expire === NULL)
            $expire = $this->expire ? $this->expire : time() + 43200;
        setcookie($name, $value, $expire, '/');

        return $this;
    }

    /**
     * Magic method set cookie
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $_COOKIE[$name] = $value;
        if ($this->expire === NULL)
            $expire = time() + 43200;
        else
            $expire = $this->expire;
        setcookie($name, $value, $expire, '/');
    }

    /**
     * Magic method get cookie
     *
     * @param $name
     *
     * @return null
     */
    public function __get($name)
    {
        if (isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        }
        return null;
    }

    /**
     * Magic method check cookie
     *
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($_COOKIE[$name]);
    }

    /**
     * Remove cookie
     *
     * @param $name
     */
    public function __unset($name)
    {
        unset($_COOKIE[$name]);
        setcookie($name, "", time() - $this->expire, '/');
    }
}