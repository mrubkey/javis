<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://xphp.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Config.php 20109 2015-20-08 12:05 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Config
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_config.html
 */
class Config
{
    /**
     * All config loaded with structure [$module][$file][$item]
     *
     * @var array
     */
    public static $items = array();

    /**
     * Cache
     *
     * @var array
     */
    public static $cache = array();

    /**
     * Check config exist
     * <code>
     *     $exists = Config::has('Database');
     * </code>
     *
     * @param $key
     *
     * @return bool
     */
    public static function has($key)
    {
        return !is_null(static::get($key));
    }

    /**
     * Get config
     * <code>
     *     $host = Config::get('Database.host');
     * </code>
     *
     * @param $key
     * @param null $default
     *
     * @return null
     */
    public static function get($key, $default = NULL)
    {
        list($module, $file, $item) = static::parse($key);

        //Loaded file
        if (!static::load($module, $file))
            return $default;

        $items = static::$items[$module][$file];
        if (is_null($item)) {
            return $items;
        } else {
            return ArrayHelper::get($items, $item, $default);
        }
    }

    /**
     * Set config value
     * <code>
     *     Config::set('Database.host', 'localhost');
     * </code>
     *
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        list($module, $file, $item) = static::parse($key);

        static::load($module, $file);

        if (is_null($item)) {
            ArrayHelper::set(static::$items[$module], $file, $value);
        } else {
            ArrayHelper::set(static::$items[$module][$file], $item, $value);
        }
    }

    /**
     * Parse key to get module, file and original key
     *
     * @param $key
     *
     * @return array
     */
    protected static function parse($key)
    {
        //Cache
        if (array_key_exists($key, static::$cache)) {
            return static::$cache[$key];
        }

        $keys = explode('::', $key);
        $module = "";

        if (count($keys) >= 2) {
            $module = $keys[0];
            $segments = explode('.', $keys[1]);
        } else {
            $segments = explode('.', $keys[0]);
        }

        if (count($segments) >= 2) {
            $parsed = array($module, $segments[0], implode('.', array_slice($segments, 1)));
        } else {
            $parsed = array($module, $segments[0], NULL);
        }

        return static::$cache[$key] = $parsed;
    }

    /**
     * Load config file
     *
     * @param $module
     * @param $file
     *
     * @return bool
     */
    public static function load($module, $file)
    {
        //Config loaded
        if (isset(static::$items[$module][$file]))
            return true;

        $config = static::file($module, $file);

        //Config contains data
        if (count($config) > 0) {
            static::$items[$module][$file] = $config;
        }

        //Load success return true else return false
        return isset(static::$items[$module][$file]);
    }

    /**
     * Get config from file
     *
     * @param $module
     * @param $file
     *
     * @return array
     */
    public static function file($module, $file)
    {
        $config = array();

        foreach (static::paths($module) as $directory) {
            if ($directory !== '' and file_exists($path = "{$directory}{$file}.php")) {
                $config = array_merge($config, require $path);
            }
        }

        return $config;
    }

    /**
     * Get all config paths in module
     *
     * @param $module
     *
     * @return array
     */
    protected static function paths($module)
    {
        Runtime::load('Router', function ($time) {
            return $time >= Directory::getFileMTimeRecursive(APPLICATION_PATH, '/((.*)Controllers$)|((.*)Controllers\/((?!\.).)*$)/i');
        });

        $path = Runtime::get('Router.Modules.' . strtolower($module) . '.Map');

        if (!$path) {
            //Get all module paths
            $dirs = array('' => APPLICATION_PATH);
            $dirs = array_merge($dirs, Directory::getSubDirectories(APPLICATION_PATH . '/' . "Modules"));
            $path = $dirs[$module];
        }

        $paths[] = $path . '/Config/';

        return $paths;
    }
}
