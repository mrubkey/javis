<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: String.php 1 2015-25-08 01:05:09 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Class String
 *
 * @author      Mr.UBKey
 * @link        http://xphp.xweb.vn/user_guide/xphp_string.html
 */
class String
{
    /**
     * MD5 encode
     *
     * @param $str
     *
     * @return string
     */
    public static function md5Encode($str)
    {
        return md5(md5($str . "javis.v1.2015"));
    }

    /**
     * MD5 encode with salt
     *
     * @param $str
     * @param $salt
     *
     * @return string
     */
    public static function md5WithSaltEncode($str, $salt)
    {
        return md5(md5($str) . $salt);
    }

    /**
     * Create random string
     *
     * @param $num
     *
     * @return string
     */
    public static function randomString($num)
    {
        $characters = 'QWERTYUIOPLKJHGFDSAZXCVBNM0123456789qwertyuioplkjhgfdsazxcvbnm';
        $string = '';
        for ($p = 0; $p < $num; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    /**
     * Secure string
     *
     * @param $s_str
     *
     * @return array|string
     */
    public static function secure($s_str)
    {
        if ((function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc())
            || (ini_get('magic_quotes_sybase') && (strtolower(ini_get('magic_quotes_sybase')) != "off"))
        ) {
            if (is_array($s_str)) {
                foreach ($s_str as $k => $v) {
                    if (is_array($v)) {
                        foreach ($v as $x => $y) {
                            if (is_array($y))
                                $s_str[$k][$x] = static::secure($y);
                            else {
                                $s_str[$k][$x] = stripslashes($y);
                            }
                        }
                    } else
                        $s_str[$k] = stripslashes($v);
                }
            } else
                $s_str = stripslashes($s_str);
        }
        return $s_str;
    }

    /**
     * Encode html string
     *
     * @param $html
     *
     * @return string
     */
    public static function encodeHtmlString($html)
    {
        $out = htmlspecialchars(
            html_entity_decode($html, ENT_QUOTES, 'UTF-8'),
            ENT_QUOTES, 'UTF-8'
        );
        return $out;
    }

    /**
     * Decode html string
     *
     * @param $html
     *
     * @return string
     */
    public static function decodeHtmlString($html)
    {
        $out = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
        return $out;
    }

    /**
     * Trim all space in string
     *
     * @param $string
     *
     * @return mixed
     */
    public static function trimAll($string)
    {
        $arrChars = array("\t", "\n", "\r");
        foreach ($arrChars as $c) {
            $string = str_replace($c, "", $string);
        }
        return $string;
    }

    /**
     * Remove sign in string
     *
     * @param $str
     *
     * @return mixed
     */
    public static function removeSign($str)
    {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return $str;
    }

    /**
     * Create seo plug from string
     *
     * @param $str
     *
     * @return mixed|string
     */
    public static function seo($str)
    {
        return static::createSlug($str);
    }

    /**
     * Check string start with character
     *
     * @param $haystack
     * @param $needle
     * @param bool|true $case
     *
     * @return bool
     */
    public static function startsWith($haystack, $needle, $case = true)
    {
        if ($case)
            return (strcmp(substr($haystack, 0, strlen($needle)), $needle) === 0);
        return (strcasecmp(substr($haystack, 0, strlen($needle)), $needle) === 0);
    }

    /**
     * Check string end with
     *
     * @param $haystack
     * @param $needle
     * @param bool|true $case
     *
     * @return bool
     */
    public static function endsWith($haystack, $needle, $case = true)
    {
        if ($case)
            return (strcmp(
                    substr($haystack, strlen($haystack) - strlen($needle)), $needle
                ) === 0);
        return (strcasecmp(
                substr($haystack, strlen($haystack) - strlen($needle)), $needle
            ) === 0);
    }

    /**
     * Xóa các đoạn mã độc tấn công XSS
     *
     * @author   : Daniel Morris
     * @copyright: Daniel Morris
     * @license  : GNU General Public License (GPL)
     *
     * @param $source string Chuỗi cần clean
     *
     * @return string
     */

    /**
     * Clean xss
     *
     * @param $source
     *
     * @return null|string
     */
    public static function xssClean($source)
    {
        $tagsMethod = 0;
        $tagsArray = array();
        $tagBlacklist = array('applet', 'body', 'bgsound', 'base', 'basefont',
            'embed', 'frame', 'frameset', 'head', 'html', 'id', 'iframe', 'ilayer',
            'layer', 'link', 'meta', 'name', 'object', 'script', 'style', 'title',
            'xml');
        $preTag = null;
        $postTag = $source;
        $tagOpen_start = strpos($source, '<');
        while ($tagOpen_start !== false) {
            $preTag .= substr($postTag, 0, $tagOpen_start);
            $postTag = substr($postTag, $tagOpen_start);
            $fromTagOpen = substr($postTag, 1);
            $tagOpen_end = strpos($fromTagOpen, '>');
            if ($tagOpen_end === false)
                break;
            $tagOpen_nested = strpos($fromTagOpen, '<');
            if (($tagOpen_nested !== false) && ($tagOpen_nested < $tagOpen_end)) {
                $preTag .= substr($postTag, 0, ($tagOpen_nested + 1));
                $postTag = substr($postTag, ($tagOpen_nested + 1));
                $tagOpen_start = strpos($postTag, '<');
                continue;
            }
            $tagOpen_nested = (strpos($fromTagOpen, '<') + $tagOpen_start + 1);
            $currentTag = substr($fromTagOpen, 0, $tagOpen_end);
            $tagLength = strlen($currentTag);
            if (!$tagOpen_end) {
                $preTag .= $postTag;
                $tagOpen_start = strpos($postTag, '<');
            }
            $tagLeft = $currentTag;
            $attrSet = array();
            $currentSpace = strpos($tagLeft, ' ');
            if (substr($currentTag, 0, 1) == "/") {
                $isCloseTag = true;
                list ($tagName) = explode(' ', $currentTag);
                $tagName = substr($tagName, 1);
            } else {
                $isCloseTag = false;
                list ($tagName) = explode(' ', $currentTag);
            }
            if ((!preg_match("/^[a-z][a-z0-9]*$/i", $tagName)) || (!$tagName)
                || ((in_array(strtolower($tagName), $tagBlacklist)))
            ) {
                $postTag = substr($postTag, ($tagLength + 2));
                $tagOpen_start = strpos($postTag, '<');
                continue;
            }
            while ($currentSpace !== false) {
                $fromSpace = substr($tagLeft, ($currentSpace + 1));
                $nextSpace = strpos($fromSpace, ' ');
                $openQuotes = strpos($fromSpace, '"');
                $closeQuotes = strpos(
                        substr($fromSpace, ($openQuotes + 1)),
                        '"'
                    ) + $openQuotes + 1;
                if (strpos($fromSpace, '=') !== false) {
                    if (($openQuotes !== false)
                        && (strpos(
                                substr($fromSpace, ($openQuotes + 1)), '"'
                            ) !== false)
                    )
                        $attr = substr($fromSpace, 0, ($closeQuotes + 1));
                    else
                        $attr = substr($fromSpace, 0, $nextSpace);
                } else
                    $attr = substr($fromSpace, 0, $nextSpace);
                if (!$attr)
                    $attr = $fromSpace;
                $attrSet[] = $attr;
                $tagLeft = substr($fromSpace, strlen($attr));
                $currentSpace = strpos($tagLeft, ' ');
            }
            $tagFound = in_array(strtolower($tagName), $tagsArray);
            if ((!$tagFound && $tagsMethod) || ($tagFound && !$tagsMethod)) {
                if (!$isCloseTag) {
                    $attrSet = self::_filterAttr($attrSet);
                    $preTag .= '<' . $tagName;
                    for ($i = 0; $i < count($attrSet); $i++)
                        $preTag .= ' ' . $attrSet[$i];
                    if (strpos($fromTagOpen, "</" . $tagName))
                        $preTag .= '>';
                    else
                        $preTag .= ' />';
                } else
                    $preTag .= '</' . $tagName . '>';
            }
            $postTag = substr($postTag, ($tagLength + 2));
            $tagOpen_start = strpos($postTag, '<');
        }
        $preTag .= $postTag;
        return $preTag;
    }

    /**
     * Clean xss from attribute
     *
     * @param $attrSet
     *
     * @return array
     */
    private static function _filterAttr($attrSet)
    {
        $attrArray = array();
        $attrMethod = 0;
        $attrBlacklist = array('action', 'background', 'codebase', 'dynsrc', 'lowsrc');
        $newSet = array();
        for ($i = 0; $i < count($attrSet); $i++) {
            if (!$attrSet[$i])
                continue;
            $attrSubSet = explode('=', trim($attrSet[$i]));
            list ($attrSubSet[0]) = explode(' ', $attrSubSet[0]);
            if ((!eregi("^[a-z]*$", $attrSubSet[0]))
                || (((in_array(
                        strtolower($attrSubSet[0]), $attrBlacklist
                    ))
                    || (substr($attrSubSet[0], 0, 2) == 'on')))
            )
                continue;
            if ($attrSubSet[1]) {
                $attrSubSet[1] = str_replace('&#', '', $attrSubSet[1]);
                $attrSubSet[1] = preg_replace('/\s+/', '', $attrSubSet[1]);
                $attrSubSet[1] = str_replace('"', '', $attrSubSet[1]);
                if ((substr($attrSubSet[1], 0, 1) == "'")
                    && (substr($attrSubSet[1], (strlen($attrSubSet[1]) - 1), 1) == "'")
                )
                    $attrSubSet[1] = substr(
                        $attrSubSet[1], 1,
                        (strlen($attrSubSet[1]) - 2)
                    );
                $attrSubSet[1] = stripslashes($attrSubSet[1]);
            }
            if (((strpos(strtolower($attrSubSet[1]), 'expression') !== false)
                    && (strtolower($attrSubSet[0]) == 'style'))
                || (strpos(strtolower($attrSubSet[1]), 'javascript:') !== false)
                || (strpos(strtolower($attrSubSet[1]), 'behaviour:') !== false)
                || (strpos(strtolower($attrSubSet[1]), 'vbscript:') !== false)
                || (strpos(strtolower($attrSubSet[1]), 'mocha:') !== false)
                || (strpos(strtolower($attrSubSet[1]), 'livescript:') !== false)
            )
                continue;
            $attrFound = in_array(strtolower($attrSubSet[0]), $attrArray);
            if ((!$attrFound && $attrMethod) || ($attrFound && !$attrMethod)) {
                if ($attrSubSet[1])
                    $newSet[] = $attrSubSet[0] . '="' . $attrSubSet[1] . '"';
                else
                    if ($attrSubSet[1] == "0")
                        $newSet[] = $attrSubSet[0] . '="0"';
                    else
                        $newSet[] = $attrSubSet[0] . '="' . $attrSubSet[0] . '"';
            }
        }
        return $newSet;
    }

    /**
     * Shorten text
     *
     * @param $text
     * @param int $maxlength
     * @param string $appendix
     *
     * @return string
     */
    public static function shortenText($text, $maxlength = 70, $appendix = "...")
    {
        if (mb_strlen($text, 'UTF-8') <= $maxlength) {
            return $text;
        }
        $text = mb_substr($text, 0, $maxlength - mb_strlen($appendix, 'UTF-8'), 'UTF-8');
        $text .= $appendix;
        return $text;
    }

    /**
     * Clean text
     *
     * @param $text
     *
     * @return string
     */
    public static function clean($text)
    {
        return strip_tags($text);
    }

    /**
     * Create seo slug from string
     *
     * @param $string
     *
     * @return mixed|string
     */
    public static function createSlug($string)
    {
        $string = static::removeAccents($string);
        $string = static::symbolsToWords($string);
        $string = strtolower($string); // Force lowercase
        $space_chars = array(
            " ", // space
            "…", // ellipsis
            "–", // en dash
            "—", // em dash
            "/", // slash
            "\\", // backslash
            ":", // colon
            ";", // semi-colon
            ".", // period
            "+", // plus sign
            "#", // pound sign
            "~", // tilde
            "_", // underscore
            "|", // pipe
        );
        foreach ($space_chars as $char) {
            $string = str_replace($char, '-', $string); // Change spaces to dashes
        }
        // Only allow letters, numbers, and dashes
        $string = preg_replace('/([^a-zA-Z0-9\-]+)/', '', $string);
        $string = preg_replace('/-+/', '-', $string); // Clean up extra dashes
        if (substr($string, -1) === '-') { // Remove - from end
            $string = substr($string, 0, -1);
        }
        if (substr($string, 0, 1) === '-') { // Remove - from start
            $string = substr($string, 1);
        }
        return $string;
    }

    /**
     * Borrowed from WordPress
     * Converts all accent characters to ASCII characters.
     *
     * If there are no accent characters, then the string given is just returned.
     *
     * @since 1.2.1
     *
     * @param string $string Text that might have accent characters
     *
     * @return string Filtered string with replaced "nice" characters.
     */
    public static function removeAccents($string)
    {
        if (!preg_match('/[\x80-\xff]/', $string)) {
            return $string;
        }
        if (static::seemsUTF8($string)) {
            $chars = array(
                // Decompositions for Latin-1 Supplement
                chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
                chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
                chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
                chr(195) . chr(135) => 'C', chr(195) . chr(136) => 'E',
                chr(195) . chr(137) => 'E', chr(195) . chr(138) => 'E',
                chr(195) . chr(139) => 'E', chr(195) . chr(140) => 'I',
                chr(195) . chr(141) => 'I', chr(195) . chr(142) => 'I',
                chr(195) . chr(143) => 'I', chr(195) . chr(145) => 'N',
                chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
                chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
                chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
                chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
                chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
                chr(195) . chr(159) => 's', chr(195) . chr(160) => 'a',
                chr(195) . chr(161) => 'a', chr(195) . chr(162) => 'a',
                chr(195) . chr(163) => 'a', chr(195) . chr(164) => 'a',
                chr(195) . chr(165) => 'a', chr(195) . chr(167) => 'c',
                chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
                chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
                chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
                chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
                chr(195) . chr(177) => 'n', chr(195) . chr(178) => 'o',
                chr(195) . chr(179) => 'o', chr(195) . chr(180) => 'o',
                chr(195) . chr(181) => 'o', chr(195) . chr(182) => 'o',
                chr(195) . chr(182) => 'o', chr(195) . chr(185) => 'u',
                chr(195) . chr(186) => 'u', chr(195) . chr(187) => 'u',
                chr(195) . chr(188) => 'u', chr(195) . chr(189) => 'y',
                chr(195) . chr(191) => 'y',
                // Decompositions for Latin Extended-A
                chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
                chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
                chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
                chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
                chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
                chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
                chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
                chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
                chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
                chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
                chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
                chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
                chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
                chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
                chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
                chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
                chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
                chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
                chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
                chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
                chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
                chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
                chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
                chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
                chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
                chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
                chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
                chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
                chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
                chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
                chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
                chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
                chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
                chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
                chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
                chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
                chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
                chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
                chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
                chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
                chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
                chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
                chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
                chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
                chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
                chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
                chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
                chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
                chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
                chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
                chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
                chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
                chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
                chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
                chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
                chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
                chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
                chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
                chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
                chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
                chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
                chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
                chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
                chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
                // Euro Sign
                chr(226) . chr(130) . chr(172) => 'E',
                // GBP (Pound) Sign
                chr(194) . chr(163) => '');
            $string = strtr($string, $chars);
        } else {
            // Assume ISO-8859-1 if not UTF-8
            $chars['in'] = chr(128) . chr(131) . chr(138) . chr(142) . chr(154) . chr(158)
                . chr(159) . chr(162) . chr(165) . chr(181) . chr(192) . chr(193) . chr(194)
                . chr(195) . chr(196) . chr(197) . chr(199) . chr(200) . chr(201) . chr(202)
                . chr(203) . chr(204) . chr(205) . chr(206) . chr(207) . chr(209) . chr(210)
                . chr(211) . chr(212) . chr(213) . chr(214) . chr(216) . chr(217) . chr(218)
                . chr(219) . chr(220) . chr(221) . chr(224) . chr(225) . chr(226) . chr(227)
                . chr(228) . chr(229) . chr(231) . chr(232) . chr(233) . chr(234) . chr(235)
                . chr(236) . chr(237) . chr(238) . chr(239) . chr(241) . chr(242) . chr(243)
                . chr(244) . chr(245) . chr(246) . chr(248) . chr(249) . chr(250) . chr(251)
                . chr(252) . chr(253) . chr(255);
            $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";
            $string = strtr($string, $chars['in'], $chars['out']);
            $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240),
                chr(254));
            $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
            $string = str_replace($double_chars['in'], $double_chars['out'], $string);
        }
        return $string;
    }

    /**
     * Borrowed from WordPress
     * Converts all accent characters to ASCII characters.
     *
     * If there are no accent characters, then the string given is just returned.
     *
     * @since 1.2.1
     *
     * @param $str
     *
     * @return bool
     */
    public static function seemsUTF8($str)
    {
        $length = strlen($str);
        for ($i = 0; $i < $length; $i++) {
            $c = ord($str[$i]);
            if ($c < 0x80) {
                $n = 0;
            } # 0bbbbbbb
            elseif (($c & 0xE0) == 0xC0) {
                $n = 1;
            } # 110bbbbb
            elseif (($c & 0xF0) == 0xE0) {
                $n = 2;
            } # 1110bbbb
            elseif (($c & 0xF8) == 0xF0) {
                $n = 3;
            } # 11110bbb
            elseif (($c & 0xFC) == 0xF8) {
                $n = 4;
            } # 111110bb
            elseif (($c & 0xFE) == 0xFC) {
                $n = 5;
            } # 1111110b
            else {
                return false;
            } # Does not match any model
            for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
                if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Convert some symbols to words in string
     *
     * @param $output
     *
     * @return mixed
     */
    public static function symbolsToWords($output)
    {
        $output = str_replace('@', ' at ', $output);
        $output = str_replace('%', ' percent ', $output);
        $output = str_replace('&', ' and ', $output);
        return $output;
    }
}