<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: View.php 1 2015-22-07 02:05:09 PM Mr.UBKey $
 */

namespace Javis;

/**
 * View
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_view.html
 */
class View
{
    /**
     * Router
     *
     * @var Router
     */
    public $router;

    /**
     * View name
     *
     * @var string
     */
    public $name;

    /**
     * Construct
     *
     * @param $router
     * @param null $name
     */
    public function __construct(&$router, $name = NULL)
    {
        $this->router = &$router;
        if ($name !== NULL)
            $this->name = $name;
    }

    public function getViewPath()
    {
        //Get path to view folder
        $folder = $this->getViewFolder();

        //Get path to mobile view folder
        $mobilePath = $this->getMobileViewPath();

        if ($mobilePath) {
            $path = $mobilePath;
        } else {
            if ($this->name) {
                if (is_file($this->name)) {
                    return $this->name;
                }
                $path = "{$folder}/{$this->name}.phtml";
            } else {
                $path = "{$folder}/{$this->router->action}.phtml";
            }
        }

        //Return path to view file
        if (is_file($path))
            return $path;
        else {
            throw new \Exception("Can't find $path");
        }
    }

    /**
     * Get path to view folder default
     *
     * @return string
     */
    public function getViewFolder()
    {
        $modulePath = Runtime::get('Router.Modules.' . strtolower($this->router->module) . '.Map');

        return "{$modulePath}/Views/{$this->router->controller}";
    }

    /**
     * Get path to mobile view default
     *
     * @return bool|string
     */
    public function getMobileViewPath()
    {
        if (Runtime::hasFlash("Application")) {

            //Get Application from config and check enable detect
            $application = Config::get("Application");

            //Get default view folder
            $folder = $this->getViewFolder();

            if ($application['mode'] == "mobile") {
                if ($this->name) {
                    $path = "{$folder}/{$this->name}";
                } else {
                    $path = "{$folder}/{$this->router->action}";
                }
                //Mobile path
                $mobilePath = "{$path}.mobile.phtml";
                //Device path
                $devicePath = "{$path}.{$application['detector']->device}.phtml";
                if (is_file($devicePath)) {
                    return $devicePath;
                }
                if (is_file($mobilePath)) {
                    return $mobilePath;
                }
            }
        }

        return false;
    }
}