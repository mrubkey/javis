<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis_Annotation
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: AnnotationAbstract.php 1 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis\Annotation;


/**
 * Class AnnotationAbstract
 *
 * @author         XWEB Dev Team
 * @link           http://javis.xweb.vn/document/javis_annotation_annotationabstract.html
 */
abstract class AnnotationAbstract
{

    /**
     * Get value of property magically
     *
     * @param $name
     *
     * @throws \Exception
     */
    public function __get($name)
    {
        throw new \Exception(get_class($this) . "::\${$name} is not declare");
    }

    /**
     * Set value of property magically
     *
     * @param $name
     * @param $value
     *
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        throw new \Exception(get_class($this) . "::\${$name} is not declare");
    }

    /**
     * Mapping properties
     *
     * @param $properties
     * @param $indexes
     */
    protected function _map(&$properties, $indexes)
    {
        foreach ($indexes as $index => $name) {
            if (isset($properties[$index])) {
                $this->$name = $properties[$index];
                unset($properties[$index]);
            }
        }
    }

    /**
     * Init annotation
     *
     * @param $properties
     */
    public function init($properties)
    {
        if ($properties) {
            foreach ($properties as $name => $value) {
                $this->$name = $value;
            }
        }
    }

    /**
     * Init annotation from runtime static call
     *
     * @param array $array
     *
     * @return mixed
     */
    static function __set_state(array $array)
    {
        $class = get_called_class();
        $tmp = new $class();
        foreach ($array as $k => $v) {
            $tmp->$k = $v;
        }
        return $tmp;
    }
}
