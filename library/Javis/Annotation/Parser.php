<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Annotation
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2014 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Parser.php 1 2015-27-07 15:05:09 Mr.UBKey $
 */

namespace Javis\Annotation;

use Javis\Annotation\AnnotationAbstract;


/**
 * Annotation Parser
 *
 * @author         XWEB Dev Team
 * @link           http://javis.xweb.vn/document/javis_annotation_parser.html
 */
class Parser
{
    const SPECIAL_CHAR = -1;

    private static $usages = array("");

    /**
     * Parse by file path
     *
     * @param $path
     *
     * @return array
     */
    public static function parse($path)
    {
        return static::parseSource(file_get_contents($path));
    }

    /**
     * Parse source code get
     *
     * @param $source
     *
     * @return array
     */
    protected static function parseSource($source)
    {
        //all attributes
        $annotations = array();
        //use
        $use = "";
        //namspace
        $namespace = "";
        //class
        $class = "";
        //start a member
        $member = false;
        //attribute of member
        $memberAnnotations = array();
        //parse file
        foreach (token_get_all($source) as $token) {
            list ($type, $str, $line) = is_array($token) ? $token : array(self::SPECIAL_CHAR, $token, null);
            switch ($type) {
                case T_NAMESPACE:
                    $member = T_NAMESPACE;
                    break;
                case T_USE:
                    $member = T_USE;
                    break;
                case T_DOC_COMMENT:
                case T_COMMENT:
                    $attr = static::getAnnotations($str);
                    if (!empty($attr)) {
                        $memberAnnotations = array_merge($memberAnnotations, $attr);
                    }
                    break;
                case T_CLASS:
                    $member = T_CLASS;
                    break;
                case T_VARIABLE:
                    if (!empty($memberAnnotations)) {
                        $annotations["{$namespace}{$class}::{$str}"] = $memberAnnotations;
                        $memberAnnotations = array();
                    }
                    break;
                case T_FUNCTION:
                    $member = T_FUNCTION;
                    break;
                case T_STRING:
                    if ($member) {
                        if ($member == T_NAMESPACE) {
                            $namespace .= $str . '\\';
                        } else {
                            if ($member == T_USE) {
                                $use .= $str . '\\';
                            } else {
                                if ($member == T_CLASS) {
                                    $class = $str;
                                    //if has $memberAttributes store in $attributes and reset $memberAttributes
                                    if (!empty($memberAnnotations)) {
                                        $annotations["{$namespace}{$class}"] = $memberAnnotations;
                                        $memberAnnotations = array();
                                    }
                                }
                                if ($member == T_FUNCTION) {
                                    if (!empty($memberAnnotations)) {
                                        $annotations["{$namespace}{$class}::{$str}()"] = $memberAnnotations;
                                        $memberAnnotations = array();
                                    }
                                }
                                $member = false;
                            }
                        }
                    }
                    break;
                //next
                case T_WHITESPACE:
                case T_PUBLIC:
                case T_PROTECTED:
                case T_PRIVATE:
                case T_ABSTRACT:
                case T_FINAL:
                case T_VAR:
                case T_REQUIRE:
                case T_REQUIRE_ONCE:
                case T_INCLUDE:
                case T_INCLUDE_ONCE:
                case self::SPECIAL_CHAR:
                case T_CONSTANT_ENCAPSED_STRING:
                    if ($use) {
                        static::$usages[] = $use;
                        $use = "";
                    }
                    break;
                default:
                    $memberAnnotations = array();
                    break;
            }
        }

        return $annotations;
    }

    /**
     * Parse comment and get attribute value
     *
     * @param $str
     *
     * @return array
     */
    protected static function getAnnotations($str)
    {
        $annos = static::_parseComment($str);
        $annotations = array();
        foreach ($annos as $a => $params) {
            //Init annotation
            foreach(static::$usages as $use) {
                $use = rtrim($use, '\\');
                $use = str_replace(array_pop(explode('\\', $use)), "", $use);
                $annoClass = Manager::getAnnotationClassName($use . $a);
                //result is class name
                if ($annoClass && is_subclass_of($annoClass, 'Javis\Annotation\AnnotationAbstract')) {
                    /* @var $annotation AnnotationAbstract */
                    $annotation = new $annoClass();
                    $annotation->init(is_array($params) ? $params : array($params));
                    //add to annotations class
                    $annotations[] = $annotation;
                    break;
                }
            }
        }
        return $annotations;
    }

    const ANNOTATION_REGEX = '/@([\w\\\\]+)(?:\s*(?:\(\s*)?(.*?)(?:\s*\))?)??\s*(?:\n|\*\/)/u';

    const PARAMETER_REGEX = '/(\w+)\s*=\s*(\[[^\]]*\]|"[^"]*"|[^,)]*)\s*(?:,|$)/u';

    /**
     * Parse comment get annotation
     *
     * @param $docComment
     *
     * @return array
     */
    private static function _parseComment($docComment)
    {
        $hasAttributes = preg_match_all(
            self::ANNOTATION_REGEX,
            $docComment,
            $matches,
            PREG_SET_ORDER
        );

        if (!$hasAttributes) {
            return array();
        }

        $attributes = array();
        foreach ($matches as $attr) {
            //Ignore annotation start with lower char
            if (substr($attr[1], 0, 1) == strtolower(substr($attr[1], 0, 1))) {
                continue;
            }

            $attrName = $attr[1];

            $val = true;
            if (isset($attr[2])) {
                $hasParams = preg_match_all(
                    self::PARAMETER_REGEX, $attr[2], $params,
                    PREG_SET_ORDER
                );
                if ($hasParams) {
                    $val = array();
                    foreach ($params as $param) {
                        $val[$param[1]] = self::_parseValue($param[2]);
                    }
                } else {
                    $val = trim($attr[2]);
                    if ($val == '') {
                        $val = true;
                    } else {
                        $val = self::_parseValue($val);
                    }
                }
            }

            if (isset($attributes[$attrName])) {
                if (!is_array($attributes[$attrName])) {
                    $attributes[$attrName] = array($attributes[$attrName]);
                }
                $attributes[$attrName][] = $val;
            } else {
                $attributes[$attrName] = $val;
            }
        }
        return $attributes;
    }

    /**
     * Parse param get value
     *
     * @param $value
     *
     * @return array|bool|mixed|string
     */
    private static function _parseValue($value)
    {
        $val = trim($value);

        if (substr($val, 0, 1) == '[' && substr($val, -1) == ']') {
            // Array values
            $vals = explode(',', substr($val, 1, -1));
            $val = array();
            foreach ($vals as $v) {
                $val[] = self::_parseValue($v);
            }
            return $val;

        } else {
            if (substr($val, 0, 1) == '{' && substr($val, -1) == '}') {
                //json object
                return json_decode($val);
            } else {
                if ((substr($val, 0, 1) == '"' && substr($val, -1) == '"')
                    || substr($val, 0, 1) == "'" && substr($val, -1) == "'"
                ) {
                    // Remove quote
                    $val = substr($val, 1, -1);
                    return self::_parseValue($val);

                } else {
                    if (strtolower($val) == 'true') {
                        // Boolean true
                        return true;

                    } else {
                        if (strtolower($val) == 'false') {
                            // Boolean false
                            return false;

                        } else {
                            if (is_numeric($val)) {
                                // Numeric float, init
                                if ((float)$val == (int)$val) {
                                    return (int)$val;
                                } else {
                                    return (float)$val;
                                }

                            } else {
                                // Return value
                                return $val;
                            }
                        }
                    }
                }
            }
        }
    }
}