<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package		Javis
 * @author		XWEB Dev Team
 * @copyright	Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license		http://javis.xweb.vn/license.html     GNU GPL License
 * @version		$Id: Partial.php 1 2015-22-07 02:05:09 PM Mr.UBKey $
 */

namespace Javis\View;


use Javis\Asset;
use Javis\Cache\Frontend;
use Javis\Cookie;
use Javis\Form;
use Javis\Html;
use Javis\Model;
use Javis\Resource;
use Javis\Router;
use Javis\Runtime;
use Javis\Session;
use Javis\Url;
use Javis\View\Layout\Content;
use Javis\View\Layout\Section;
use Javis\Widget;

/**
 * Class Partial
 *
 * @author		Mr.UBKey
 * @link		http://javis.xweb.vn/user_guide/javis_view_partial.html
 */
class Partial
{
    /**
     * File partial
     *
     * @var string
     */
    public $file;

    /**
     * View Data
     *
     * @var \stdClass
     */
    public $data;

    /**
     * @var Model
     */
    public $model;

    /**
     * @var Html
     */
    protected $html;

    /**
     * @var Form
     */
    protected $form;

    /**
     * @var Url
     */
    protected $url;

    /**
     * @var Frontend
     */
    protected $cache;

    /**
     * @var Section
     */
    protected $section;


    /**
     * @var Content
     */
    protected $content;


    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Cookie
     */
    protected $cookie;

    /**
     * @var Resource
     */
    protected $resource;

    /**
     * @var Asset
     */
    protected $asset;

    /**
     * @var Widget
     */
    protected $widget;

    /**
     * @var Router
     */
    public $router;
    
    /**
     * Render partial
     */
    public function render()
    {
    	//Session
    	$this->session = Session::getInstance();
    	
    	//Cookie
    	$this->cookie = Cookie::getInstance();
    	
    	//HTML
    	$this->html = new Html($this->router, $this);
    	
    	//Form
    	$this->form = new Form($this->router, $this);
    	
    	//URL
    	$this->url = new Url($this->router);

    	//Widget
    	$this->widget = new Widget();
    	
    	//Resource
    	if(Runtime::hasFlash("DefaultResource"))
    		$this->resource = Runtime::get("DefaultResource");
    	
        //Extract data
        if(is_object($this->data))
        	extract(get_object_vars($this->data), EXTR_OVERWRITE);
        else
        	extract($this->data, EXTR_OVERWRITE);

        //Include file
        include $this->file;
    }
}