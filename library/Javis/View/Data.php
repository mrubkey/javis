<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        XPHP\View
 * @author         XWEB Dev Team
 * @author         Petr Trofimov <petrofimov@yandex.ru>
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://xphp.xweb.vn/license.html     GNU GPL License
 * @version        $Id: View.php 1 2015-03-02 11:18:09 AM Mr.UBKey $
 */

namespace Javis\View;

/**
 * Class Data
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_view_data.html
 */
class Data extends \stdClass
{
    public function __set($name, $value)
    {
        if (is_callable($value)) {
            return $this->$name = new fn(array('constructor' => $value));
        } else {
            $this->$name = $value;
        }
    }
}

/**
 * Base class for creating dynamic objects
 *
 * @author Petr Trofimov <petrofimov@yandex.ru>
 * @see https://github.com/ptrofimov/jslikeobject
 */
class fn
{
    /** @var array */
    private $properties = array();

    public function __construct(array $properties = array())
    {
        $this->properties = $properties;
    }

    public function __get($key)
    {
        $value = null;
        if (array_key_exists($key, $this->properties)) {
            $value = $this->properties[$key];
        } elseif (isset($this->properties['prototype'])) {
            $value = $this->properties['prototype']->{$key};
        }
        return $value;
    }

    public function __set($key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function __call($method, array $args)
    {
        return is_callable($this->{$method})
            ? call_user_func_array(
                $this->{$method}->bindTo($this),
                $args
            ) : null;
    }

    public function __invoke($x)
    {
        $instance = new static($this->properties);
        $result = false;
        if ($this->constructor) {
            $result = call_user_method_array("constructor", $instance, func_get_args());
        }
        return $result === false || $result === null ? $instance : $result;
    }
}