<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Section.php 1 2015-20-06 02:05:09 PM Mr.UBKey $
 */

namespace Javis\View\Layout;

/**
 * Class Section
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_layout_section.html
 */
class Section
{
    /**
     * Array contains section data
     *
     * @var array
     */
    public $section;

    /**
     * Section name
     *
     * @var string
     */
    public $name;


    /**
     * Begin a section
     *
     * @param $name
     *
     * @return bool
     */
    public function begin($name)
    {
        //Check a section need call end
        if ($this->name)
            trigger_error('Must end() before begin() new section.', E_USER_ERROR);

        $this->name = $name;

        ob_start();

        return true;
    }

    /**
     * End a section
     */
    public function end()
    {
        //Check begin section
        if (!$this->name)
            trigger_error('Need begin() before end() section.', E_USER_ERROR);

        //Create section
        $this->section[$this->name] = ob_get_contents();

        //Clean buffer
        ob_end_clean();

        //Clean section
        $this->name = null;
    }

    /**
     * Get section content
     *
     * @param $name
     *
     * @return string
     */
    public function __get($name)
    {
        if (isset($this->section[$name]))
            return $this->section[$name];
        else
            return "";
    }
}