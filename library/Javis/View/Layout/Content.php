<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Content.php 1 2015-20-06 02:05:09 PM Mr.UBKey $
 */

namespace Javis\View\Layout;

/**
 * Class Content
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_view_layout_content.html
 */
class Content
{
    /**
     * Content from buffer
     *
     * @var string
     */
    private $content;

    /**
     * Status of begin block content
     *
     * @var bool
     */
    private $_status;

    public function begin()
    {
        if ($this->content)
            trigger_error('Only once begin().', E_USER_ERROR);

        //Open output buffer
        ob_start();

        $this->_status = true;
    }

    public function end()
    {
        if (!$this->_status)
            trigger_error('Need begin() before end().', E_USER_ERROR);

        //Set content
        $this->content = ob_get_contents();

        //Clean ob
        ob_end_clean();
    }

    public function set($content)
    {
        $this->content = $content;
    }

    public function __toString()
    {
        if ($this->content === NULl)
            $this->content = "";

        return $this->content;
    }
}