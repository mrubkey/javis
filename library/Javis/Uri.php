<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category       XPHP
 * @package        Uri
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://xphp.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Uri.php 1 2015-10-08 4:36 PM Mr.UBKey $
 */

namespace Javis;

/**
 * Class Uri
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_uri.html
 */
class Uri
{
	/**
	 * Uri
	 *
	 * @var string
	 */
	protected static $uri;

	/**
	 * Segments
	 *
	 * @var array
	 */
	protected static $segments = array();

	/**
	 * Get uri with query string
	 *
	 * @return string
	 */
	public static function getRequestUri()
	{
		if (!isset($_SERVER['REQUEST_URI'])) {
			$_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);
			if (isset($_SERVER['QUERY_STRING'])) {
				$_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
			}
		}

		return $_SERVER['REQUEST_URI'];
	}

	/**
	 * Get path info of request
	 *
	 * @return string
	 */
	public static function getUri()
	{
		if (static::$uri) {
			return static::$uri;
		}

		$uri = static::getRequestUri();
		if (isset($_SERVER['QUERY_STRING'])) {
			$uri = str_replace('?' . $_SERVER['QUERY_STRING'], '', $uri);
		}

		$uri = static::formatUri($uri);

		static::segments($uri);

		return static::$uri = $uri;
	}

	/**
	 * Format uri string
	 *
	 * @param $uri
	 *
	 * @return string
	 */
	protected static function formatUri($uri)
	{
		return trim($uri, '/') ? : '/';
	}

	/**
	 * Get segments
	 * <code>
	 *     $segment = URI::segment(1);
	 * </code>
	 *
	 * @param null $index
	 * @param null $default
	 *
	 * @return array|null
	 */
	public static function getSegments($index = NULL, $default = NULL)
	{
		static::getUri();

		if ($index === NULL) {
			return static::$segments;
		}
		else {
			return ArrayHelper::get(static::$segments, $index - 1, $default);
		}
	}

	/**
	 * Parse uri to segments
	 *
	 * @param $uri
	 */
	protected static function segments($uri)
	{
		$segments = explode('/', trim($uri, '/'));

		static::$segments = array_diff($segments, array(''));
	}
}
