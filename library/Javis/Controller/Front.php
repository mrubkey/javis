<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis\Controller
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://xphp.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Front.php 1 2015-21-08 02:05:09 AM Mr.UBKey $
 */

namespace Javis\Controller;

use Javis\Action\Result\ResultAbstract;
use Javis\Action\Result\ResultInterface;
use Javis\Annotation;
use Javis\Controller;
use Javis\Directory;
use Javis\Loader;
use Javis\Router;
use Javis\Runtime;
use Javis\Model;


/**
 * Class Front
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_controller_front.html
 */
class Front
{
    /**
     * Construct
     *
     * @param Router|NULL $router
     */
    public function __construct(Router $router = NULL)
    {
        if ($router === NULL) {
            //Router
            $this->router = Router::getInstance();
            $this->router->analysisRequestUri();
        } else {
            $this->router = $router;
        }

        $this->loadModule($this->router->module);

        Runtime::load('Action', function ($time) {
            return $time >= Directory::getFileMTimeRecursive(APPLICATION_PATH, '/((.*)Controllers$)|((.*)Controllers\/((?!\.).)*$)/i');
        });

        //Get action info
        if (!Runtime::get('Action')) {
            $this->_detectActionParams();
        }
    }

    /**
     * Dispatch Front Controller
     *
     * @param \Closure $errorFunction
     *
     * @throws \Exception
     */
    public function dispatch($errorFunction = NULL)
    {
        try {
            if ($this->router->isEmpty())
                throw new \Exception("Page not found", 404);

            //Controller Class
            $class = Runtime::get("Router.Modules." . strtolower($this->router->module)
                . ".Controllers." . strtolower($this->router->controller)
                . ".Class");

            //Call action get action result
            if (class_exists($class))
                $obj = new $class($this->router);
            else
                throw new \Exception("Controller not found", 404);

            //Result
            $result = false;

            //Call special action
            $actionMethods = Runtime::get("Router.Modules." . strtolower($this->router->module)
                . ".Controllers." . strtolower($this->router->controller)
                . ".Actions." . strtolower($this->router->action) . '.Methods');

            //Request is ajax
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' && isset($actionMethods['ajax'])) {
                $result = $this->_callAction($obj, $this->router->action . $actionMethods['ajax']);
            }

            if ($result === false) {
                $method = $_SERVER['REQUEST_METHOD'];
                switch ($method) {
                    case 'PUT':
                        if (isset($actionMethods['put'])) {
                            $result = & $this->_callAction($obj, $this->router->action . $actionMethods['put']);
                        }
                        break;
                    case 'POST':
                        if (isset($actionMethods['post'])) {
                            $result = & $this->_callAction($obj, $this->router->action . $actionMethods['post']);
                        }
                        break;
                    case 'GET':
                        if (isset($actionMethods['get'])) {
                            $result = & $this->_callAction($obj, $this->router->action . $actionMethods['get']);
                        }
                        break;
                    case 'HEAD':
                        if (isset($actionMethods['head'])) {
                            $result = & $this->_callAction($obj, $this->router->action . $actionMethods['head']);
                        }
                        break;
                    case 'DELETE':
                        if (isset($actionMethods['delete'])) {
                            $result = & $this->_callAction($obj, $this->router->action . $actionMethods['delete']);
                        }
                        break;
                    case 'OPTIONS':
                        if (isset($actionMethods['options'])) {
                            $result = & $this->_callAction($obj, $this->router->action . $actionMethods['options']);
                        }
                        break;
                }
            }

            //Check result
            if (!$result) {
                //Xử lý gọi action
                if (isset($actionMethods['action'])) {
                    $result = & $this->_callAction($obj, $this->router->action . $actionMethods['action']);
                } else if ($result === false) {
                    throw new \Exception("Action not found", 404);
                }
            }

            //Result interface
            if ($result !== NULL && $result instanceof ResultInterface) {
                if ($result instanceof ResultAbstract) {
                    $model = $obj->getModel();
                    $result->setModel($model);
                    $result->setRouter($this->router);
                    $result->setViewData($obj->view);
                    $result->setLayoutFile($obj->getLayout());
                }
                //Execute result
                $result->execute();
            }
        } catch (\Exception $ex) {
            if ($errorFunction !== NULL && isset($errorFunction[$ex->getCode()])) {
                $func = $errorFunction[$ex->getCode()];
                $func($this->router);
                $this->dispatch($errorFunction);
            } else
                throw $ex;
        }
    }

    /**
     * Call action in controller with fire all event
     *
     * @param Controller $controller
     * @param $action
     *
     * @return mixed
     */
    private function _callAction(&$controller, $action)
    {
        //Get action params
        $actionParams = Runtime::get('Action.' . $this->router->module . '.' . $this->router->controller . '.' . $action);

        //Params
        $params = array();

        if ($actionParams) {
            foreach ($actionParams as $p => $type) {
                if ($type) {
                    if ($type === 'array') {
                        $params[] = $this->router->args;
                    } else {
                        $obj = new $type;
                        //If param is instance of model
                        if ($obj instanceof Model) {
                            $controller->setModel($obj);
                            $controller->parseModelParams();
                        }
                        $params[] = & $obj;
                    }
                }
            }
        }

        //Get all annotations of action
        $annotations = Annotation::ofMethod($controller, $action, '\\Javis\\Action\\Annotation\\AnnotationAbstract');

        //Set controller to annotations
        foreach ($annotations as $a) {
            /* @var  $a \Javis\Action\Annotation\AnnotationAbstract */
            $a->setController($controller);
        }

        //Fire event onActionExecute
        foreach ($annotations as $a) {
            /* @var  $a \Javis\Action\Annotation\AnnotationAbstract */
            $a->onActionExecute();
        }

        //Call action with params
        $result = call_user_func_array(array($controller, $action), $params);

        //Fire event onActionExecuted with param ActionResult
        foreach ($annotations as $a) {
            /* @var  $a \Javis\Action\Annotation\AnnotationAbstract */
            $a->onActionExecuted($result);
        }

        //Free controller
        foreach ($annotations as $a) {
            /* @var  $a \Javis\Action\Annotation\AnnotationAbstract */
            $a->freeController();
        }

        return $result;
    }

    /**
     * Get info of all action save to runtime
     */
    private function _detectActionParams()
    {
        $modules = Runtime::get("Router.Modules");
        $mcap = array();
        foreach ($modules as $m) {
            Loader::registerNamespaces(array($m['Name'] => $m['Map']));
            $cap = array();
            foreach ($m['Controllers'] as $c) {
                $actionParams = array();
                foreach ($c['Actions'] as $action => $a) {
                    foreach ($a['Methods'] as $me) {
                        $method = new \ReflectionMethod($c['Class'], $action . $me);
                        $params = $method->getParameters();
                        if ($params) {
                            $paramWithTypes = array();
                            foreach ($params as $p) {
                                if ($p->isArray()) {
                                    $paramWithTypes[$p->getName()] = 'array';
                                } else {
                                    if ($p->getClass() instanceof \ReflectionClass)
                                        $paramWithTypes[$p->getName()] = $p->getClass()->getName();
                                    else
                                        $paramWithTypes[$p->getName()] = $p->getClass();
                                }

                            }
                            $actionParams[$method->getName()] = $paramWithTypes;
                        } else
                            $actionParams[$method->getName()] = false;
                    }
                }
                $cap[$c['Name']] = $actionParams;
            }
            $mcap[$m['Name']] = $cap;
        }

        //Save to runtime
        Runtime::set('Action', $mcap);
        Runtime::save('Action');
    }

    /**
     * Load module
     *
     * @param $module
     */
    public function loadModule($module)
    {
        Loader::registerNamespaces(array($module => Runtime::get('Router.Modules.' . strtolower($module) . '.Map')));
    }
}