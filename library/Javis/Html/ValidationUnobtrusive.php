<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: ValidationUnobtrusive.php 1 2015-15-09 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Html;


use Javis\Annotation;
use Javis\Model;

/**
 * Class ValidationUnobtrusive
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_html_validationunobtrusive.html
 */
class ValidationUnobtrusive
{
	/**
	 * Html attributes
	 *
	 * @var array
	 */
	private $_attributes;
	
	/**
	 * Model
	 *
	 * @var Model
	 */
	private $_model;

	/**
	 * @param $viewResult
	 */
	public function  __construct($viewResult)
	{
		$this->_model = &$viewResult->model;
		$this->_attributes = array();
	}

	/**
	 * Get property name of attribute
	 *
	 * @param $property
	 *
	 * @return mixed
	 */
	public function get($property)
	{
		//Data value
		$this->_attributes[$property]["data-val"] = "true";
		
		//Get all Model Annotation Validation of property
		$annotations = Annotation::ofProperty($this->_model, $property, 'Javis\\Model\\Annotation\\Validation\\ValidationAbstract');
				
		foreach ($annotations as $ann)
		{
			$ruleName = $ann->name;
			$this->$ruleName($property, $ann);
		}
				
		return $this->_attributes[$property];
	}

	/**
	 * Required
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function required($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-required'] = $attribute->message;		
	}

	/**
	 * Minlength
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function minlength($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-length'] = $attribute->message;
		$this->_attributes[$propertyName]['data-val-length-min'] = $attribute->minlength;
	}

	/**
	 * Maxlength
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function maxlength($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-length'] = $attribute->message;
		$this->_attributes[$propertyName]['data-val-length-max'] = $attribute->maxlength;	
	}

	/**
	 * Rangelength
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function rangelength($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-length'] = $attribute->message;
		$this->_attributes[$propertyName]['data-val-length-min'] = $attribute->min;
		$this->_attributes[$propertyName]['data-val-length-max'] = $attribute->max;
	}

	/**
	 * Min
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function min($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-range'] = $attribute->message;
		$this->_attributes[$propertyName]['data-val-range-min'] = $attribute->min;
	}

	/**
	 * Max
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function max($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-range'] = $attribute->message;
		$this->_attributes[$propertyName]['data-val-range-max'] = $attribute->max;
	}

	/**
	 * Range
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function range($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-range'] = $attribute->message;
		$this->_attributes[$propertyName]['data-val-range-min'] = $attribute->min;
		$this->_attributes[$propertyName]['data-val-range-max'] = $attribute->max;
	}

	/**
	 * Email
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function email($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-email'] = $attribute->message;
	}

	/**
	 * Url
	 *
	 * @param $propertyName
	 * @param $attribute
	 */
	public function url($propertyName, $attribute)
	{
		$this->_attributes[$propertyName]['data-val-url'] = $attribute->message;
	}
	
	public function date()
	{}
	
	public function dateISO()
	{}
	
	public function dateDE()
	{}
	
	public function number()
	{}
	
	public function numberDE()
	{}
	
	public function digits()
	{}
	
	public function creditcard()
	{}
	
	public function accept()
	{}
	
	public function equalTo()
	{}
}