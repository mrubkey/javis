<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Validation.php 1 2015-15-09 02:05:09 PM Mr.UBKey $
 */

namespace Javis\Html;

use Javis\Annotation;
use Javis\Model;

/**
 * Class Validation
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/user_guide/javis_html_validation.html
 */
class Validation
{
    /**
     * Error messages
     * @var array
     */
    private $_messages = array();

    /**
     * Validation rules
     *
     * @var array
     */
    private $_rules = array();

    /**
     * Form name
     *
     * @var string
     */
    private $_formName;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        //Get model properties
        $properties = $model->getModelProperties();
        foreach ($properties as $p) {
            //Get all Model Attribute Validation
            $annotations = Annotation::ofProperty($model, $p, '\\Javis\\Model\\Annotation\\Validation\\ValidationAbstract');
            foreach ($annotations as $a) {
                $ruleName = $a->name;
                $this->$ruleName($p, $a);
            }
        }
    }

    /**
     * Required
     *
     * @param $propertyName
     * @param $attribute
     */
    public function required($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['required'] = true;
        $this->_messages[$propertyName]['required'] = $attribute->message;
    }

    /**
     * Minlength
     *
     * @param $propertyName
     * @param $attribute
     */
    public function minlength($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['minlength'] = $attribute->minlength;
        $this->_messages[$propertyName]['minlength'] = $attribute->message;
    }

    /**
     * Maxlength
     *
     * @param $propertyName
     * @param $attribute
     */
    public function maxlength($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['maxlength'] = $attribute->maxlength;
        $this->_messages[$propertyName]['maxlength'] = $attribute->message;
    }

    /**
     * Rangelength
     *
     * @param $propertyName
     * @param $attribute
     */
    public function rangelength($propertyName, $attribute)
    {
        $minlength = (int)$attribute->min;
        $maxlength = (int)$attribute->max;

        $this->_rules[$propertyName]['rangelength'] = array($minlength, $maxlength);
        $this->_messages[$propertyName]['rangelength'] = $attribute->message;
    }

    /**
     * Min
     *
     * @param $propertyName
     * @param $attribute
     */
    public function min($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['min'] = $attribute->min;
        $this->_messages[$propertyName]['min'] = $attribute->message;

    }

    /**
     * Max
     *
     * @param $propertyName
     * @param $attribute
     */
    public function max($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['max'] = $attribute->max;
        $this->_messages[$propertyName]['max'] = $attribute->message;
    }

    /**
     * Range
     *
     * @param $propertyName
     * @param $attribute
     */
    public function range($propertyName, $attribute)
    {
        $min = (int)$attribute->min;
        $max = (int)$attribute->max;

        $this->_rules[$propertyName]['range'] = array($min, $max);
        $this->_messages[$propertyName]['range'] = $attribute->message;
    }

    /**
     * Email
     *
     * @param $propertyName
     * @param $attribute
     */
    public function email($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['email'] = true;
        $this->_messages[$propertyName]['email'] = $attribute->message;
    }

    /**
     * Url
     *
     * @param $propertyName
     * @param $attribute
     */
    public function url($propertyName, $attribute)
    {
        $this->_rules[$propertyName]['url'] = true;
        $this->_messages[$propertyName]['url'] = $attribute->message;
    }

    public function date()
    {
    }

    public function dateISO()
    {
    }

    public function dateDE()
    {
    }

    public function number()
    {
    }

    public function numberDE()
    {
    }

    public function digits()
    {
    }

    public function creditcard()
    {
    }

    public function accept()
    {
    }

    public function equalTo()
    {
    }

    public function getScript()
    {
        $jsonRules = json_encode($this->_rules);
        $jsonMessages = json_encode($this->_messages);
        $script = "<script type='text/javascript'>" . "\n";
        $script .= "//<![CDATA[" . "\n";
        $script .= "jQuery(function(){ $('form";
        if (!empty($this->_formName))
            $script .= "#" . $this->_formName;
        $script .= "').validate({rules: $jsonRules, messages: $jsonMessages}) });" . "\n";
        $script .= "//]]>" . "\n";
        $script .= "</script>" . "\n";

        return $script;
    }

    public function setFormName($formName)
    {
        $this->_formName = $formName;
    }
}