<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        XPHP
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Base.php 1 2015-20-09 02:05:09 AM Mr.UBKey $
 */

namespace Javis\Html;


/**
 * Class Base
 *
 * @package		  Javis\Html
 * @author        Mr.UBKey
 * @link          http://javis.xweb.vn/user_guide/javis_html_base.html
 */
class Base
{
	/**
	 * Parse array to html string of attribute
	 *
	 * @param $arrAttr
	 *
	 * @return string
	 */
	public static function htmlAttributes($arrAttr)
	{
		$attr = '';
		foreach($arrAttr as $name => $value)
			$attr .= "$name=\"$value\" ";
		return $attr;
	}
}