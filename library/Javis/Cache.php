<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Cache
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Cache.php 1 2015-27-07 02:05:09 Mr.UBKey $
 */

namespace Javis;

use Javis\Cache\CacheAbstract;

/**
 * Cache
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/document/javis_cache.html
 */
class Cache extends CacheAbstract
{

    /**
     * Class of cache
     *
     * @var string
     */
    public $class;

    /**
     * Check php system has cache extension
     *
     * @var bool
     */
    public $hasCacheExtension = true;

    /**
     * Cache instance from adaptor
     *
     * @var CacheAbstract
     */
    private $cacheObject;

    /**
     * Static instance of cache
     *
     * @var Cache
     */
    private static $instance;

    /**
     * Static instance of cache with extension
     *
     * @var Cache
     */
    private static $exInstance;

    /**
     * Get instance
     *
     * @return Cache
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Get instance with extension
     *
     * @return Cache
     */
    public static function getExtensionCacheInstance()
    {
        if (!isset(self::$exInstance)) {
            self::$exInstance = new self(NULL, false);
        }

        return self::$exInstance;
    }

    /**
     * @param null $class
     * @param bool|true $fileInclude
     */
    public function __construct($class = NULL, $fileInclude = true)
    {
        if ($class !== NULL)
            $this->class = $class;
        else {
            if (extension_loaded('apc'))
                $this->class = "\\Javis\\Cache\\APC";
            else if (extension_loaded('xcache'))
                $this->class = "\\Javis\\Cache\\XCache";
            else if (extension_loaded('memcache')) {
                $this->class = "\\Javis\\Cache\\Memcache";
                $class = $this->class;
                $this->hasCacheExtension = $class::connect();
            } else if ($fileInclude) {
                $this->class = "\\Javis\\Cache\\File";
                $this->hasCacheExtension = false;
            }
        }
        if ($this->class)
            $this->load($this->class);
    }

    /**
     * Load cache from adaptor
     *
     * @param $class
     */
    public function load($class)
    {
        $this->cacheObject = new $class($this->getTime(), $this->compresses, $this->getPrefix(), $this->getPath());
    }

    public function __isset($key)
    {
        return isset($this->cacheObject->$key);
    }

    public function __unset($key)
    {
        unset($this->cacheObject->$key);
    }

    public function __get($key)
    {
        return $this->cacheObject->$key;
    }

    public function __set($key, $value)
    {
        $this->cacheObject->$key = $value;
    }

    /**
     * Get cache
     *
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->cacheObject->$key;
    }

    /**
     * Set cache
     *
     * @param $key
     * @param $value
     * @param null $time
     *
     * @return $this
     */
    public function set($key, $value, $time = NULL)
    {
        $this->cacheObject->set($key, $value, $time);
        return $this;
    }
}