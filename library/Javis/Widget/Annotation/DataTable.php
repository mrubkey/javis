<?php

namespace Javis\Widget\Annotation;

use Javis\Annotation\AnnotationAbstract;

/**
 * \\XPHP\\Widget\\Attribute\\DataTable BASE ON Ignited Datatables
 *
 * This is a wrapper class/library based on the native Datatables server-side implementation by Allan Jardine
 * found at http://datatables.net/examples/data_sources/server_side.html for CodeIgniter
 *
 * @package    CodeIgniter
 * @subpackage libraries
 * @category   library
 * @version    0.7
 * @author     Vincent Bambico <metal.conspiracy@gmail.com>
 *             Yusuf Ozdemir <yusuf@ozdemir.be>
 *             XWeb Dev Team <mr.ubkey@gmail.com>
 * @link       http://codeigniter.com/forums/viewthread/160896/
 * @Annotation
 * @Target("METHOD")
 */
class DataTable extends AnnotationAbstract
{
    /**
     * Các columns
     * @var array
     */
    public $columns;

    public $name;

    protected $filter = array();

    /**
     * @var \XPHP\DataSource
     */
    protected $dataSource;

    /**
     * Gán tham số truyền vào cho Attribute
     * @throws \Exception
     */
    public function init($properties)
    {
        if (isset($properties['name']))
            $this->name = $properties['name'];
        if (isset($properties['columns']))
            $this->columns = $properties['columns'];
    }

    /**
     * Thực thi XPHP_Widget_DataTable và trả về kết quả
     *
     * @param \XPHP\Model | \XPHP\DataSource | NULL $result
     *
     * @throws \Exception
     */
    public function onActionExecuted(&$result = NULL)
    {
        if ($result instanceof DataSource)
            $dataSource = $result;
        else if ($result instanceof Model) {
            $dataSource = new DataSource($result, NULL);
        } else {
            if (empty($this->name))
                throw new \Exception("Bạn phải gán giá trị name cho DataTable hoặc action trả về \\XPHP\\DataSource");
            else
                $dataSource = new DataSource($this->name);
        }
        if ($this->columns)
            $dataSource->select(str_replace(' ', ',', $this->columns));
        //Set datasource
        $this->dataSource = $dataSource;
        //Set isAsso
        $this->dataSource->isAssociative = $this->check_mDataprop();
        $this->dataSource->bind();
        echo $this->generate();
        die;
    }

    /**
     * Builds all the necessary query segments and performs the main query based on results set from chained statements
     *
     * @param string $charset
     * @return string
     */
    public function generate($charset = 'UTF-8')
    {
        $this->get_paging();
        $this->get_ordering();
        $this->get_filtering();
        return $this->produce_output($charset);
    }

    /**
     * Generates the LIMIT portion of the query
     *
     * @return mixed
     */
    protected function get_paging()
    {
        $iStart = $this->_request('iDisplayStart');
        $iLength = $this->_request('iDisplayLength');
        $this->dataSource->limit(($iLength && $iLength != '' && $iLength != '-1') ? $iLength : 100, ($iStart) ? $iStart : 0);
    }

    /**
     * Generates the ORDER BY portion of the query
     *
     * @return mixed
     */
    protected function get_ordering()
    {
        if ($this->check_mDataprop())
            $mColArray = $this->get_mDataprop();
        else if ($this->_request('sColumns'))
            $mColArray = explode(',', $this->_request('sColumns'));
        else
            $mColArray = $this->dataSource->columnNames;

        $mColArray = array_values(array_diff($mColArray, $this->dataSource->unsetColumns));
        $columns = array_values(array_diff($this->dataSource->columnNames, $this->dataSource->unsetColumns));

        if ($this->_request('iSortingCols'))
            $this->dataSource->resetOrderBy(); #Xoá các sắp xếp mặc định
        for ($i = 0; $i < intval($this->_request('iSortingCols')); $i++) {
            if (isset($mColArray[intval($this->_request('iSortCol_' . $i))])
                && in_array($mColArray[intval($this->_request('iSortCol_' . $i))], $columns)
                && $this->_request('bSortable_' . intval($this->_request('iSortCol_' . $i))) == 'true'
            ) {
                $this->dataSource->order_by($mColArray[intval($this->_request('iSortCol_' . $i))], $this->_request('sSortDir_' . $i));
            }
        }
    }

    /**
     * Tạo bộ lọc FILTER
     *
     * @param mixed $key_condition
     * @param string $val
     * @param bool $backtick_protect
     *
     * @return \XPHP\DataSource
     */
    public function filter($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
        $this->filter[] = array($key_condition, $val, $backtick_protect);
        return $this;
    }

    /**
     * Generates the LIKE portion of the query
     *
     * @return mixed
     */
    protected function get_filtering()
    {
        if ($this->check_mDataprop()) {
            $mColArray = $this->get_mDataprop();
        } else {
            if ($this->_request('sColumns')) {
                $mColArray = explode(',', $this->_request('sColumns'));
            } else {
                $mColArray = $this->dataSource->columnNames;
            }
        }

        $sWhere = '';
        $sSearch = mysql_real_escape_string($this->_request('sSearch'));
        $mColArray = array_values(array_diff($mColArray, $this->dataSource->unsetColumns));
        $columns = array_values(array_diff($this->dataSource->columnNames, $this->dataSource->unsetColumns));

        if ($sSearch != '') {
            for ($i = 0; $i < count($mColArray); $i++) {
                if ($this->_request('bSearchable_' . $i) == 'true' && in_array($mColArray[$i], $columns)) {
                    $sWhere .= $mColArray[$i] . " LIKE '%" . $sSearch . "%' OR ";
                }
            }
        }

        $sWhere = substr_replace($sWhere, '', -3);

        if ($sWhere != '') {
            $this->dataSource->where('(' . $sWhere . ')');
        }

        $sRangeSeparator = $this->_request('sRangeSeparator');
        $sBetweenSeparator = $this->_request('sBetweenSeparator');
        $sArrayRegx = '\[(.*)\]';

        for ($i = 0; $i < intval($this->_request('iColumns')); $i++) {
            if ($this->_request('sSearch_' . $i) && $this->_request('sSearch_' . $i) != ''
                && in_array(
                    $mColArray[$i], $columns
                )
            ) {
                $searchVal = $this->_request('sSearch_' . $i);
                if (preg_match("/{$sArrayRegx}/i", trim($searchVal), $matches)) {
                    if (!empty($matches[1])) {
                        $in = str_replace('"', "'", $matches[1]);
                        $inQuery = "{$mColArray[$i]} IN ({$in})";
                        $this->dataSource->where($inQuery);
                    }
                } else {
                    $miSearch = explode(',', $searchVal);

                    foreach ($miSearch as $val) {
                        if (preg_match("/(<=|>=|=|<|>)(\s*)(.+)/i", trim($val), $matches)) {
                            $this->dataSource->where($mColArray[$i] . ' ' . $matches[1], $matches[3]);
                        } else {
                            if (!empty($sRangeSeparator)
                                && preg_match(
                                    "/(.*)$sRangeSeparator(.*)/i", trim($val), $matches
                                )
                            ) {
                                $rangeQuery = '';

                                if (!empty($matches[1])) {
                                    $rangeQuery = 'STR_TO_DATE(' . $mColArray[$i]
                                        . ",'%d/%m/%y %H:%i:%s') >= STR_TO_DATE('" . $matches[1]
                                        . " 00:00:00','%d/%m/%y %H:%i:%s')";
                                }

                                if (!empty($matches[2])) {
                                    $rangeQuery
                                        .= (!empty($rangeQuery) ? ' AND ' : '') . 'STR_TO_DATE('
                                        . $mColArray[$i]
                                        . ",'%d/%m/%y %H:%i:%s') <= STR_TO_DATE('"
                                        . $matches[2] . " 23:59:59','%d/%m/%y %H:%i:%s')";
                                }

                                if (!empty($matches[1]) || !empty($matches[2])) {
                                    $this->dataSource->where($rangeQuery);
                                }
                            } else {
                                if (!empty($sBetweenSeparator)
                                    && preg_match(
                                        "/(.*)$sBetweenSeparator(.*)/i", trim($val), $matches
                                    )
                                ) {
                                    if (!empty($matches[1]) && !empty($matches[2])) {
                                        if (is_numeric($matches[1]) && is_numeric($matches[2])) {
                                            $betweenQuery = "$mColArray[$i] BETWEEN $matches[1] AND $matches[2]";
                                        } else {
                                            $betweenQuery = "$mColArray[$i] BETWEEN '$matches[1]' AND '$matches[2]'";
                                        }
                                        if (!empty($betweenQuery)) {
                                            $this->dataSource->where($betweenQuery);
                                        }
                                    }
                                } else {
                                    $this->dataSource->where($mColArray[$i] . ' LIKE', '%' . $val . '%');
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($this->filter as $val) {
            $this->dataSource->where($val[0], $val[1], $val[2]);
        }
    }

    /**
     * Builds a JSON encoded string data
     *
     * @param string $charset
     *
     * @return string
     */
    protected function produce_output($charset)
    {
        $aaData = $this->dataSource->getDataTable();
        $iTotal = $this->get_total_results();
        $iFilteredTotal = $this->get_total_results(true);

        $sColumns = array_diff($this->dataSource->columnNames, $this->dataSource->unsetColumns);
        $sColumns = array_merge_recursive($sColumns, array_merge(array_keys($this->dataSource->addColumns), array_keys($this->dataSource->addTemplateColumns)));

        $sOutput = array
        (
            'sEcho' => intval($this->_request('sEcho')),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => $aaData,
            'sColumns' => implode(',', $sColumns)
        );

        return json_encode($sOutput);
    }

    /**
     * Get result count
     *
     * @param bool $filtering
     *
     * @return integer
     */
    protected function get_total_results($filtering = false)
    {
        if ($filtering)
            $this->get_filtering();

        return $this->dataSource->countResults($filtering);
    }

    /**
     * Check mDataprop
     *
     * @return bool
     */
    protected function check_mDataprop()
    {
        if (!$this->_request('mDataProp_0'))
            return false;

        for ($i = 0; $i < intval($this->_request('iColumns')); $i++)
            if (!is_numeric($this->_request('mDataProp_' . $i)))
                return true;

        return false;
    }

    /**
     * Get mDataprop order
     *
     * @return mixed
     */
    protected function get_mDataprop()
    {
        $mDataProp = array();

        for ($i = 0; $i < intval($this->_request('iColumns')); $i++)
            $mDataProp[] = $this->_request('mDataProp_' . $i);

        return $mDataProp;
    }

    private function _request($name)
    {
        return isset($_REQUEST[$name]) ? $_REQUEST[$name] : false;
    }
}