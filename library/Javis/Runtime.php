<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package        Javis
 * @author         XWEB Dev Team
 * @copyright      Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license        http://javis.xweb.vn/license.html     GNU GPL License
 * @version        $Id: Runtime.php 1 2015-27-07 10:37 AM Mr.UBKey $
 */

namespace Javis;

/**
 * Lớp Runtime
 *
 * @author         Mr.UBKey
 * @link           http://javis.xweb.vn/user_guide/javis_runtime.html
 */
class Runtime
{
    /**
     * Static data in runtime
     *
     * @var array
     */
    private static $_items = array();

    /**
     * Load runtime
     * <code>
     *     Runtime::load('Router')
     *     Runtime::load('Router', function ($time, $file) { \\magic trick })
     *     Runtime::load('Router', function ($time, $file) { \\magic trick }, $file)
     * </code>
     * @param  string $item
     * @param \Closure|null $trigger
     * @param  mixed|string $file
     *
     */
    public static function load($item, \Closure $trigger = NULL, $file = NULL)
    {
        //In static data
        if (isset(static::$_items[$item])) {
            if ($trigger !== NULL) {
                if (!$trigger(static::$_items[$item]['time'], static::$_items[$item]['file'])) {
                    unset(static::$_items[$item]);
                }
            }
            return;
        } else {
            $cache = Cache::getInstance();
            $cacheKey = $_SERVER['SERVER_NAME'] . "::Runtime:{$item}";
            $timeKey = "time_" . $_SERVER['SERVER_NAME'] . "::Runtime:{$item}";
            if ($cache->hasCacheExtension) {
                if (isset($cache->$cacheKey) && isset($cache->$timeKey)) {
                    if ($trigger !== NULL) {
                        if (!$trigger($cache->$timeKey, false)) {
                            return;
                        }
                    }
                    static::$_items[$item]['data'] = $cache->$cacheKey;
                    static::$_items[$item]['time'] = $cache->$timeKey;
                    return;
                }
            }
            if ($file !== NULL && is_file($file)) {
                $time = filemtime($file);
                if ($trigger !== NULL) {
                    if (!$trigger($time, $file)) {
                        return;
                    }
                }
                static::$_items[$item]['file'] = $file;
                static::$_items[$item]['data'] = require $file;
                static::$_items[$item]['time'] = $time;
                if ($cache->hasCacheExtension) {
                    $cache->set($cacheKey, static::$_items[$item]['data'], 0);
                    $cache->set($timeKey, static::$_items[$item]['time'], 0);
                }
                return;
            }
            if (is_file($path = STORAGE_PATH . "/Runtime/{$item}.php")) {
                $time = filemtime($path);
                if ($trigger !== NULL) {
                    if (!$trigger($time, false)) {
                        return;
                    }
                }
                static::$_items[$item]['file'] = $path;
                static::$_items[$item]['data'] = require $path;
                static::$_items[$item]['time'] = $time;
                if ($cache->hasCacheExtension) {
                    $cache->set($cacheKey, static::$_items[$item]['data'], 0);
                    $cache->set($timeKey, static::$_items[$item]['time'], 0);
                }
                return;
            }
        }
    }

    /**
     * Get data from runtime
     * <code>
     *     $modules = Runtime::get('Router.Modules');
     * </code>
     *
     * @param $key
     *
     * @return null
     */
    public static function get($key)
    {
        $keys = explode('.', $key);
        if (count($keys) >= 2) {
            $item = $keys[0];
            unset($keys[0]);
            return isset(static::$_items[$item]['data']) ? ArrayHelper::get(static::$_items[$item]['data'], $keys, NULL) : NULL;
        } else {
            return isset(static::$_items[$keys[0]]['data']) ? static::$_items[$keys[0]]['data'] : NULL;
        }
    }

    /**
     * Save runtime
     * <code>
     *     Runtime::save('Router.Modules');
     * </code>
     *
     * @param $key
     *
     * @return bool
     */
    public static function save($key)
    {
        if (static::$_items[$key] !== NULL) {
            if (!isset(static::$_items[$key]['file'])) {
                static::$_items[$key]['file'] = STORAGE_PATH . "/Runtime/{$key}.php";
            }

            $parts = explode('/', static::$_items[$key]['file']);
            $file = array_pop($parts);
            $parts = implode('/', $parts);

            if (!is_dir($parts))
                mkdir($parts, 0777, true);

            file_put_contents(
                "{$parts}/{$file}",
                "<?php return " . var_export(static::$_items[$key]['data'], true) . '; ?>'
            );

            static::$_items[$key]['time'] = filemtime("{$parts}/{$file}");

            return true;
        }
        return false;
    }

    /**
     * Set value to runtime
     * <code>
     *     Runtime::set('Router.Modules', $data);
     * </code>
     *
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        $cache = Cache::getInstance();
        $keys = explode('.', $key);

        if (count($keys) >= 2) {
            $item = $keys[0];
            unset($keys[0]);
            static::$_items[$item]['data'] = array();
            ArrayHelper::set(static::$_items[$item]['data'], $keys, $value);
            if ($cache->hasCacheExtension) {
                $cacheKey = $_SERVER['SERVER_NAME'] . "::Runtime:{$item}";
                $timeKey = "time_" . $_SERVER['SERVER_NAME'] . "::Runtime:{$item}";
                $cache->set($cacheKey, static::$_items[$item]['data'], 0);
                $cache->set($timeKey, time(), 0);
            }
            return;
        } else {
            static::$_items[$keys[0]]['data'] = $value;
            if ($cache->hasCacheExtension) {
                $cacheKey = $_SERVER['SERVER_NAME'] . "::Runtime:{$keys[0]}";
                $timeKey = "time_" . $_SERVER['SERVER_NAME'] . "::Runtime:{$keys[0]}";
                $cache->set($cacheKey, static::$_items[$keys[0]]['data'], 0);
                $cache->set($timeKey, time(), 0);
            }
            return;
        }
    }

    /**
     * Set flash value to runtime. Unable to save
     *
     * <code>
     *     Runtime::setFlash('Database.connect', $data);
     * </code>
     *
     * @param $key
     * @param $value
     */
    public static function setFlash($key, &$value)
    {
        $keys = explode('.', $key);

        if (count($keys) >= 2) {
            $item = $keys[0];
            unset($keys[0]);
            static::$_items[$item]['data'] = array();
            ArrayHelper::set(static::$_items[$item]['data'], $keys, $value);
            return;
        } else {
            static::$_items[$keys[0]]['data'] = $value;
            return;
        }
    }

    /**
     * Check flash exists in runtime
     *
     * <code>
     *     Runtime::hasFlash('Database.connect');
     * </code>
     *
     * @param $key
     * @return bool
     */
    public static function hasFlash($key)
    {
        $keys = explode('.', $key);

        if (count($keys) >= 2) {
            $item = $keys[0];
            unset($keys[0]);
            return isset(static::$_items[$item]);
        } else {
            return isset(static::$_items[$keys[0]]['data']);
        }
    }
}
