<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         Javis\Annotation
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://javis.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Annotation.php 1 2015-27-07 11:08:09 Mr.UBKey $
 */

namespace Javis;

use Javis\Annotation\Manager;

/**
 * Annotation
 *
 * @author          Mr.UBKey
 * @link            http://javis.xweb.vn/document/javis_annotation.html
 */
class Annotation
{
    /**
     * Get annotations of class
     *
     * @param $class
     * @param null $type
     *
     * @return mixed
     */
    public static function ofClass($class, $type = NULL)
    {
        return Manager::getClassAnnotations($class, $type);
    }

    /**
     * Get annotations of method
     *
     * @param $class
     * @param null $method
     * @param null $type
     *
     * @return mixed
     */
    public static function ofMethod($class, $method = NULL, $type = NULL)
    {
        return Manager::getMethodAnnotations($class, $method, $type);
    }

    /**
     * Get annotation of property
     *
     * @param $class
     * @param null $property
     * @param null $type
     *
     * @return mixed
     */
    public static function ofProperty($class, $property = NULL, $type = NULL)
    {
        return Manager::getPropertyAnnotations($class, $property, $type);
    }
}