<?php
/**
 * Javis Framework
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @package         XPHP
 * @author          XWEB Dev Team
 * @copyright       Copyright (c) 2010-2011 XWEB. (http://xweb.vn)
 * @license         http://xphp.xweb.vn/license.html     GNU GPL License
 * @version         $Id: Controller.php 1 2015-27-08 02:05:09 PM Mr.UBKey $
 */

namespace Javis;

use XPHP\Action\Result\Json;
use XPHP\Action\Result\Redirect;
use XPHP\Action\Result\View;

/**
 * Class Controller
 *
 * @author      Mr.UBKey
 * @link        http://javis.xweb.vn/user_guide/javis_controller.html
 */
abstract class Controller
{
    /**
     * Object contains data set from controller to view
     * @var \stdClass
     */
    public $view;

    /**
     * Layout
     *
     * @var string
     */
    protected $layout;

    /**
     * Url helper
     *
     * @var Url
     */
    protected $url;

    /**
     * Cache helper
     *
     * @var Cache
     */
    protected $cache;

    /**
     * Resources helper
     *
     * @var Resource
     */
    protected $resource;

    /**
     * Session helper
     *
     * @var Session
     */
    protected $session;

    /**
     * Cookie helper
     *
     * @var Cookie
     */
    protected $cookie;

    /**
     * Router
     *
     * @var Router
     */
    protected $router;

    /**
     * Model
     *
     * @var Model
     *
     * @abstract Model
     */
    protected $model;

    /**
     * Params of request from _GET, _POST, ARGS
     *
     * @var array
     */
    protected $params = array();

    /**
     * Constructor
     *
     * @param Router $router
     */
    public function __construct(&$router)
    {
        //Gán router cho controller
        $this->router = $router;

        //Lấy toàn bộ các tham số truyền vào dạng POST VÀ GET đưa vào $this->params
        if (isset($_POST)) {
            foreach ($_POST as $key => $value) {
                $this->params[$key] = String::secure($value);
            }
        }
        if (isset($_GET)) {
            foreach ($_GET as $key => $value) {
                $this->params[$key] = String::secure($value);
            }
        }
        //Lấy toàn bộ các tham số truyền vào dạng Args đưa vào $this->params
        if ($this->router->args) {
            foreach ($this->router->args as $a => $v) {
                $this->params[$a] = $v;
            }
        }

        $this->view = new \stdClass();

        //Khởi tạo lớp hỗ trợ cache mặc định tự nhận các thông số
        $this->cache = Cache::getInstance();

        //Session
        $this->session = Session::getInstance();

        //Cookie
        $this->cookie = Cookie::getInstance();

        //Xử lý resource
        //if (Registry::isRegistered("DefaultResource"))
            //$this->resource = Registry::get("DefaultResource");

        //Url
        $this->url = new Url($router);

        //Call method _init after constructor
        //Method _init() run before all action in controller
        if (method_exists(get_class($this), '_init'))
            $this->_init();
    }

    /**
     * Get current router
     *
     * @return Router
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * Check params exists
     *
     * @param null $param
     *
     * @return bool
     */
    public function hasParams($param = NULL)
    {
        if ($param !== NULL) {
            if (isset($this->params[$param])) {
                return true;
            } else {
                return false;
            }
        } else {
            if (sizeof($this->params) > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Get params value
     *
     * @param null $param
     *
     * @return array|bool
     */
    public function getParams($param = NULL)
    {
        if ($param !== NULL) {
            if (isset($this->params[$param])) {
                return $this->params[$param];
            } else {
                return false;
            }
        }
        return $this->params;
    }

    /**
     * Set model
     *
     * @param Model $obj
     *
     * @throws \Exception
     */
    public function setModel(&$obj)
    {
        if (is_object($obj)) {
            if (empty($this->model)) {
                $this->model = $obj;
            }
        } else {
            throw new \Exception("Param model is not object");
        }
    }

    /**
     * Get model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Load other module to current module. If autoload all module through it
     *
     * @param $module
     */
    public function loadModule($module)
    {
        Loader::registerNamespaces(array($module => Runtime::get('Router.Modules.' . strtolower($module) . '.Map')));
    }

    /**
     * Kiểm tra xem model có dạng giống tham số truyền vào không
     * VD: Nếu người dùng POST dữ liệu phương thức sẽ kiểm tra xem có dạng
     * giống với model truyền vào không
     * Nếu kiểm tra thành công gọi phương thức parseModelParams phân tich và gán
     * các giá trị tương ứng vào $this->model
     * Trả về true, false
     *
     * @param $obj \XPHP\Model Truyền vào đối tượng thuộc lớp \XPHP\Model để kiểm tra
     *
     * @throws \Exception
     * @return bool
     */
    protected function hasModel($obj = NULL)
    {
        if ($obj === NULL) {
            if (empty($this->model)) {
                throw new \Exception("Model chưa được định kiểu bạn cần phải truyền vào tham số là đối tượng cho hasModel()");
            } else {
                $obj = $this->model;
            }
        }
        if (is_object($obj)) {
            if (get_parent_class($obj) != "\\XPHP\\Model") {
                throw new \Exception("Kiểu của model phải được kế thừa từ lớp \\XPHP\\Model");
                return;
            }
            $ref = new \ReflectionClass($obj);
            $refproperties = $ref->getProperties();
            //Mảng lưu trữ các thuộc tính
            $arrProperties = array();
            foreach ($refproperties as $refproperty) {
                $arrProperties[] = $refproperty->getName();
            }
            //Nếu là ajax request
            if ($this->isAjaxRequest() && isset($_POST["jsondata"])) {
                $arr = json_decode($_POST["jsondata"], true);
                $arrData = array();
                foreach ($arr as $key => $value) {
                    $arrData[$key] = $value;
                }
                //Kiểm tra các thuộc tính với arrData
                foreach ($arrProperties as $property) {
                    if (isset($arrData[$property])) {
                        //Gán đối tượng làm model
                        $this->model = $obj;
                        //Gọi tới phương thức phân tích để gán giá trị vào model
                        $this->parseModelParams();
                        //Have property
                        return true;
                    } else {
                        continue;
                    }
                }
            } //Nếu người dùng POST dữ liệu
            else {
                //Kiểm tra thuộc tính với các tham số người dùng POST lên
                foreach ($arrProperties as $property) {
                    if (isset($this->params[$property])) {
                        //Gán đối tượng làm model
                        $this->model = $obj;
                        //Gọi tới phương thức phân tích để gán giá trị vào model
                        $this->parseModelParams();
                        return true;
                    } else {
                        continue;
                    }
                }
                return false;
            }
        } else {
            throw new \Exception("Phương thức hasModel() tham số truyền vào là một đối tượng kế thừa từ lớp \\XPHP\\Model");
        }
    }

    /**
     * Phân tích tham số truyền vào từ POST, GET, AJAX để gán các giá trị tương ứng vào model
     *
     * @access public
     *
     * @return boolean
     */
    public function parseModelParams()
    {
        //Đánh dấu model đã được gán thuộc tính
        $hasProperty = false;
        if (is_object($this->model)) {
            //Lấy ra tên các property của model
            $ref = new \ReflectionClass($this->model);
            $refProperties = $ref->getProperties();
            //Mảng lưu trữ các thuộc tính
            $propertyNames = array();
            foreach ($refProperties as $refProperty) {
                $propertyNames[] = $refProperty->getName();
            }
            //Nếu người dùng sử dụng POST
            //Kiểm tra thuộc tính với các tham số POST từ client
            //Gán các giá trị tương ứng vào model
            foreach ($propertyNames as $property) {
                if (isset($this->params[$property])) {
                    $this->model->$property = String::secure($this->params[$property]);
                    $hasProperty = true;
                } else {
                    continue;
                }
            }
        }
        return $hasProperty;
    }

    /**
     * Load layout từ Controller
     *
     * @param null|string $layout
     */
    protected function loadLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * Bỏ layout đã được load giống loadLayout(null)
     */
    protected function unloadLayout()
    {
        $this->layout = NULL;
    }

    /**
     * Lấy ra layout đã được load
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Trả về \XPHP\Action\Result\View
     *
     * @param mixed $name  null | string | \XPHP\Model | Truyền vào tên view cần render hoặc đối tượng lớp thừa kế từ lớp \XPHP\Model
     * @param mixed $model  null | \XPHP\Model | Lớp thừa kế từ lớp \XPHP\Model
     *
     * @return \XPHP\Action\Result\View
     */
    protected function view($name=NULL, $model = NULL)
    {
        $render = $name;
        if($name !== NULL && $name instanceof Model)
        {
            $this->setModel($name);
            $render = NULL;
            $model  = NULL;
        }
        if ($model !== NULL && $model instanceof Model) {
            $this->setModel($model);
        }
        return new View($render);
    }

    /**
     * Trả về kết quả là đối tượng của lớp \XPHP\Action\Result\Redirect
     * $action- Tên action, $controller - Tên controller, $module - Tên module, $args - Tham số
     *
     * @param mixed $mixed
     *
     * @return \XPHP\Action\Result\Redirect
     */
    public function redirect($mixed)
    {
        return new Redirect(call_user_func_array(array(& $this->url, 'action'), func_get_args()));
    }

    /**
     * Trả về kết quả là đối tượng của lớp \XPHP\Action\Result\Redirect
     *
     * @param string $url Đường dẫn cần redirect tới
     *
     * @return \XPHP\Action\Result\Redirect
     */
    protected function redirectUrl($url)
    {
        return new Redirect($url);
    }

    /**
     * Trả về kết quả là đối tượng của lớp \XPHP\Action\Result\Json
     *
     * @param mixed $data Dữ liệu cần trả về kiểu json
     *
     * @return \XPHP\Action\Result\Json
     */
    protected function json($data)
    {
        return new Json($data);
    }
}